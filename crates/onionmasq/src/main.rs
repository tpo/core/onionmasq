use anyhow::Context;
use clap::{Arg, ArgMatches, Command};
use log::{info, trace, warn};
use onion_tunnel::config::{TunnelConfig, TurnAuth, TurnConfig};
use onion_tunnel::scaffolding::{
    ConnectionDetails, FailedConnectionDetails, UdpTunnelChoice, UdpTunnelContext,
};
use onion_tunnel::{CountryCode, IpEndpoint, OnionTunnel, TunnelScaffolding};
use simple_proc_net::ProcNetEntry;
use std::collections::HashMap;
use std::ffi::c_void;
use std::mem::size_of;
use std::net::SocketAddr;
use std::os::fd::AsRawFd;
use std::os::fd::RawFd;
use std::path::PathBuf;
use std::str::FromStr;
use std::sync::Arc;
use std::{fs, io};
use tokio::net::TcpSocket;
use tracing_subscriber::FmtSubscriber;

struct LinuxScaffolding {
    cc: Option<CountryCode>,
    can_mark: bool,
    log_connections: bool,
    udp_config: UdpTunnelChoice,
}

impl LinuxScaffolding {
    /// The fwmark to set on arti connections (so policy routing can avoid them being routed
    /// recursively back into the tunnel).
    ///
    /// This is just a randomly generated set of 2 bytes that hopefully won't conflict with
    /// anything else.
    pub const FWMARK: libc::c_int = 0xc185;

    /// Mark the provided socket file descriptor with the `FWMARK`.
    #[cfg(target_os = "linux")]
    fn mark_fd(fd: RawFd) -> io::Result<()> {
        let ret = unsafe {
            libc::setsockopt(
                fd,
                libc::SOL_SOCKET,
                libc::SO_MARK,
                &Self::FWMARK as *const libc::c_int as *const c_void,
                size_of::<libc::c_int>() as _,
            )
        };
        if ret != 0 {
            Err(io::Error::last_os_error())
        } else {
            Ok(())
        }
    }
}

impl TunnelScaffolding for LinuxScaffolding {
    fn protect(&self, fd: RawFd, _: &SocketAddr) -> io::Result<()> {
        #[cfg(target_os = "linux")]
        if self.can_mark {
            Self::mark_fd(fd)?;
        }
        Ok(())
    }

    fn locate(&self, _: IpEndpoint, _: IpEndpoint, _: u64) -> Option<CountryCode> {
        self.cc
    }

    fn on_bootstrapped(&self) {
        info!("Connection to Tor complete!");
    }

    fn on_established(&self, details: ConnectionDetails<'_>) {
        if self.log_connections {
            let exit_identity = details
                .circuit_relays()
                .last()
                .map(|x| {
                    let cc = match x.country_code {
                        Some(v) => v.to_string(),
                        None => "??".into(),
                    };
                    if let Some(i) = x.ed_identity {
                        format!("{} ({})", i, cc)
                    } else if let Some(i) = x.rsa_identity {
                        format!("{} ({})", i, cc)
                    } else {
                        "???".into()
                    }
                })
                .unwrap_or_else(|| "???".into());

            info!(
                "New connection to '{}' (uid {}) via exit {}",
                details.tor_dst, details.isolation_key, exit_identity
            );
        }
    }

    fn on_arti_failure(&self, details: FailedConnectionDetails) {
        if self.log_connections {
            warn!(
                "Failed to connect to '{}': {}",
                details.tor_dst, details.error
            );
        }
    }

    fn isolate(&self, src: IpEndpoint, dst: IpEndpoint, ip_proto: u8) -> io::Result<u64> {
        let iter = match ip_proto {
            6 => ProcNetEntry::tcp4()?.chain(ProcNetEntry::tcp6()?),
            17 => ProcNetEntry::udp4()?.chain(ProcNetEntry::udp6()?),
            x => panic!("unknown ip_proto {x}"),
        };
        for entry in iter {
            match entry? {
                Ok(v) => {
                    if IpEndpoint::from(v.local_addr) == src
                        && IpEndpoint::from(v.remote_addr) == dst
                    {
                        trace!("isolated {src} -> {dst} proto {ip_proto} as {}", v.uid);
                        return Ok(v.uid as u64);
                    }
                }
                Err(e) => {
                    warn!("Failed to parse /proc line: {}", e.offending_line);
                    warn!("Error encountered: {}", e.error);
                }
            }
        }
        warn!("no isolation found for {src} -> {dst} proto {ip_proto}");
        Ok(0)
    }

    fn udp_tunnel(&self, context: &UdpTunnelContext) -> UdpTunnelChoice {
        info!("requesting new UDP tunnel for {:?}", context);
        self.udp_config.clone()
    }

    fn udp_prebuild(&self, isolation_key: u64) -> HashMap<UdpTunnelChoice, usize> {
        const NUM_EXTRA_TUNNELS: usize = 2;
        trace!(
            "maintaining pre-built UDP tunnels, request {:?} for isolation {:?}",
            NUM_EXTRA_TUNNELS,
            isolation_key
        );
        let choice = self.udp_config.clone();
        let mut result: HashMap<UdpTunnelChoice, usize> = Default::default();
        result.insert(choice, NUM_EXTRA_TUNNELS);
        result
    }
}

/// Obtain the TURN shared secret in the following fashion:
/// 1. If given, read the file at `turn-secret-file`
/// 2. If given, use `turn-secret`
/// 3. Return [`TurnAuth::None`]
fn get_turn_secret(matches: &ArgMatches) -> anyhow::Result<TurnAuth> {
    if let Some(turn_secret_path) = matches.get_one::<String>("turn-secret-file") {
        // TODO: Consider checking the permissions of `turn_secret_path`,
        // although this might increase the complexity and portability of the
        // code in a non-justified fashion.
        let secret = fs::read_to_string(turn_secret_path)?
            .to_string()
            .trim()
            .to_string();
        Ok(TurnAuth::Secret(secret))
    } else if let Some(turn_secret) = matches.get_one::<String>("turn-secret") {
        Ok(TurnAuth::Secret(turn_secret.clone()))
    } else {
        Ok(TurnAuth::None)
    }
}

const ENV_FILTER_VERBOSE: &str =
    "info,smoltcp=trace,onion_tunnel=trace,arti_client=debug,tor_chanmgr=debug,tor_proto=debug";
const ENV_FILTER_TAME: &str = "info";

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    let matches = Command::new("onionmasq")
        .version(env!("CARGO_PKG_VERSION"))
        .author("eta <eta@torproject.org>")
        .about("A magical TUN device that feeds traffic via Tor.")
        .arg(
            Arg::new("arti-config")
            .short('a')
            .long("arti-config")
            .value_name("PATH")
            .help("Path to a manual arti.toml configuration")
        )
        .arg(
            Arg::new("disable-fs-permission-checks")
            .long("disable-fs-permission-checks")
            .help("Don't check permissions on the files we use")
            .action(clap::ArgAction::SetTrue),
        )
        .arg(
            Arg::new("tun-device")
                .short('d')
                .long("device")
                .value_name("DEVICE")
                .default_value("onion0")
                .help(
                    "Name of a TUN device to use. Will attempt to create one if it doesn't exist.",
                ),
        )
        .arg(
            Arg::new("country-code")
                .short('c')
                .long("country-code")
                .value_name("DE|NL|etc")
                .help("Make traffic come from exit nodes in this country (ISO 3166-1 alpha-2 country code)"),
        )
        /* TODO(eta): make this a thing
        .arg(Arg::new("whole-system")
            .short('s')
            .long("whole-system")
            .help("Make all system traffic go through the onionmasq TUN device. Off by default.")
            .action(clap::ArgAction::SetTrue)
        )
         */
        .arg(
            Arg::new("turn-server")
            .long("turn-server")
            .value_name("HOST:PORT")
            .help("TURN relay server to contact through Tor, for tunneling UDP traffic")
        )
        .arg(
            Arg::new("turn-secret")
            .long("turn-secret")
            .value_name("STRING")
            .help("Shared secret for TURN server authorization")
        )
        .arg(
            Arg::new("turn-secret-file")
            .long("turn-secret-file")
            .value_name("PATH")
            .help("Path to a file containing the shared secret for TURN server authorization")
        )
        .arg(Arg::new("debug")
            .long("debug")
            .help("Enable detailed debug logging. Off by default.")
            .action(clap::ArgAction::SetTrue)
        )
        .arg(Arg::new("verbose")
            .short('v')
            .long("verbose")
            .help("Print information about successful and failed connections. Off by default.")
            .action(clap::ArgAction::SetTrue))
        .get_matches();

    let mut onion_cfg: TunnelConfig = Default::default();
    onion_cfg.arti_cfg_path = matches
        .get_one::<String>("arti-config")
        .map(|p| PathBuf::from_str(p).expect("invalid path supplied"));

    onion_cfg.disable_fs_permission_checks = matches.get_flag("disable-fs-permission-checks");

    let filter = if matches.get_flag("debug") {
        ENV_FILTER_VERBOSE
    } else {
        ENV_FILTER_TAME
    };

    let log_connections = matches.get_flag("verbose");

    let country_code = matches
        .get_one("country-code")
        .map(|x: &String| CountryCode::from_str(x))
        .transpose()
        .context("invalid country code provided")?;

    let tun_device: &String = matches.get_one("tun-device").unwrap();

    // We obtain this information regardless of the presence of a turn-server,
    // because we always want to inform about security warnings related to the
    // `turn-secret` option.
    let turn_auth = get_turn_secret(&matches)?;

    let udp_config = match matches.get_one::<String>("turn-server") {
        None => Default::default(),
        Some(server) => {
            let mut config =
                TurnConfig::new(server).context("TURN server isn't a valid Tor address")?;
            config.auth = turn_auth;
            UdpTunnelChoice::Turn(Arc::new(config))
        }
    };

    FmtSubscriber::builder().with_env_filter(filter).init();

    info!(
        "Starting onionmasq {} on device '{}'...",
        env!("CARGO_PKG_VERSION"),
        tun_device
    );

    // Warn if `turn-secret was supplied`
    if matches.get_one::<String>("turn-secret").is_some() {
        warn!("Providing --turn-secret opens a security vulnerability, consider using --turn-secret-file exclusively");
    }

    // Check whether we can call setsockopt(). If we can't, don't bother doing so in future, since
    // we probably don't have the capabilities; just print an explanatory warning instead.
    #[cfg(target_os = "linux")]
    let dummy_socket = TcpSocket::new_v4()?;
    #[cfg(target_os = "linux")]
    let can_mark = match LinuxScaffolding::mark_fd(dummy_socket.as_raw_fd()) {
        Ok(_) => {
            info!(
                "Outgoing connections (to the Tor network) will use fwmark {:#x}.",
                LinuxScaffolding::FWMARK
            );
            true
        }
        Err(e) => {
            warn!("Calling setsockopt() failed: {e}");
            warn!("Setting fwmarks on outgoing sockets will be disabled.");
            warn!("Make the onionmasq binary CAP_NET_ADMIN to fix this problem.");
            false
        }
    };

    #[cfg(not(target_os = "linux"))]
    let can_mark = false;

    let scaffolding = LinuxScaffolding {
        can_mark,
        cc: country_code,
        log_connections,
        udp_config,
    };

    #[cfg(target_os = "linux")]
    let mut onion_tunnel = OnionTunnel::new(scaffolding, "onion0", onion_cfg).await?;

    info!("Connecting to Tor...");

    #[cfg(target_os = "linux")]
    tokio::select! {
        _ = onion_tunnel.run() => (),
        _ = tokio::signal::ctrl_c() => {
            info!("Received ^C; stopping tunnel.");
        }
    };

    Ok(())
}
