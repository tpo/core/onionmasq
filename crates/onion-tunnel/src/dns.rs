//! Implementing a DNS resolver for proxied Tor traffic.

use std::collections::HashMap;
#[cfg(feature = "udp-tunnel")]
use std::net::IpAddr;
use std::net::{Ipv4Addr, Ipv6Addr};
use std::sync::{Arc, Mutex};

use crate::errors::TunnelResult;
use crate::runtime::OnionTunnelArtiRuntime;
#[cfg(feature = "udp-tunnel")]
use crate::udp_tunnel::UdpPrebuildRequest;
use arti_client::TorClient;
#[cfg(feature = "udp-tunnel")]
use arti_client::{IsolationToken, StreamPrefs};
use bimap::BiMap;
use bytes::Bytes;
use dns_message_parser::question::QType;
use dns_message_parser::rr::RR;
use dns_message_parser::RCode;
use futures::stream::StreamExt;
use rand::RngCore;
use smoltcp::socket::udp;
use smoltcp::wire::IpEndpoint;
use smoltcp::{
    socket::udp::PacketBuffer as UdpSocketBuffer, socket::udp::PacketMetadata as UdpPacketMetadata,
    wire::IpAddress, wire::Ipv4Address, wire::Ipv6Address,
};
#[cfg(feature = "udp-tunnel")]
use tokio::sync::mpsc;
use tracing::{info, trace, warn};

use crate::socket::UdpSocket;
use crate::{OnionTunnel, TunnelScaffolding};

pub(crate) type LockedDnsCookies = Arc<Mutex<DnsCookies>>;

#[cfg(feature = "udp-tunnel")]
type ResolverResult = Result<Vec<IpAddr>, arti_client::Error>;

/// A mapping between dispensed fake IP addresses and hostnames.
///
/// This mapping is specific to a given isolation key.
/// Real IPs are optional, and obtained asynchronously.
/// For UDP tunneling we must convert between real and fake IPs.
///
struct IsolatedCookies {
    #[cfg(feature = "udp-tunnel")]
    isolation_token: IsolationToken,
    map_v4: BiMap<Ipv4Address, String>,
    map_v6: BiMap<Ipv6Address, String>,
    #[cfg(feature = "udp-tunnel")]
    resolver_map: HashMap<String, ResolverResult>,
    #[cfg(feature = "udp-tunnel")]
    resolver_send: mpsc::Sender<(String, ResolverResult)>,
    #[cfg(feature = "udp-tunnel")]
    resolver_recv: mpsc::Receiver<(String, ResolverResult)>,
}

impl IsolatedCookies {
    fn new() -> Self {
        // Arbitrary small capacity; could be as low as 1, but larger values
        // could be a slight optimization.
        #[cfg(feature = "udp-tunnel")]
        let (resolver_send, resolver_recv) = mpsc::channel(16);
        Self {
            #[cfg(feature = "udp-tunnel")]
            isolation_token: IsolationToken::new(),
            map_v4: Default::default(),
            map_v6: Default::default(),
            #[cfg(feature = "udp-tunnel")]
            resolver_map: Default::default(),
            #[cfg(feature = "udp-tunnel")]
            resolver_send,
            #[cfg(feature = "udp-tunnel")]
            resolver_recv,
        }
    }

    fn canonicalize_hostname(hostname: &mut String) {
        // TODO: This is incomplete
        if hostname.ends_with('.') {
            hostname.pop();
        }
    }

    /// Test whether an IP is in the correct range to possibly be an output from make_for_v4/make_for_v6
    #[cfg(feature = "udp-tunnel")]
    fn ip_in_randomized_range(addr: &IpAddress) -> bool {
        match addr {
            IpAddress::Ipv4(v4) => v4.0[0] == 10,
            IpAddress::Ipv6(v6) => v6.0[..2] == [0xfe, 0xc0],
        }
    }

    /// Generate a random IPv4 address in the 10.0.0.0/8 subnet.
    ///
    /// Returns the IPv4 address or [`None`], if the address space is exhausteed.
    fn make_for_v4<S: TunnelScaffolding>(
        &mut self,
        #[allow(unused)] arti: Option<&TorClient<OnionTunnelArtiRuntime<S>>>,
        mut hostname: String,
    ) -> Option<Ipv4Address> {
        Self::canonicalize_hostname(&mut hostname);

        // Check if we've already generated one.
        if let Some(a) = self.map_v4.get_by_right(&hostname) {
            return Some(*a);
        }

        #[cfg(feature = "udp-tunnel")]
        if let Some(arti) = arti {
            self.begin_resolve(arti.clone(), hostname.clone());
        }

        // Make a random IPv4 address in the range 10.0.0.0/8.
        let mut ret = Ipv4Address([10, 0, 0, 0]);

        // 16777214 = 2^24 - 2, the total amount of IPv4 addresses in 10.0.0.0/8.
        if self.map_v4.len() == 16777214 {
            return None;
        }

        loop {
            rand::thread_rng().fill_bytes(&mut ret.0[1..]);
            if !self.map_v4.contains_left(&ret) {
                break;
            }
        }
        assert!(!self.map_v4.contains_left(&ret));

        let _ = self.map_v4.insert_no_overwrite(ret, hostname);
        Some(ret)
    }

    fn make_for_v6<S: TunnelScaffolding>(
        &mut self,
        #[allow(unused)] arti: Option<&TorClient<OnionTunnelArtiRuntime<S>>>,
        mut hostname: String,
    ) -> Option<Ipv6Address> {
        Self::canonicalize_hostname(&mut hostname);

        // Check if we've already generated one.
        if let Some(a) = self.map_v6.get_by_right(&hostname) {
            return Some(*a);
        }

        #[cfg(feature = "udp-tunnel")]
        if let Some(arti) = arti {
            self.begin_resolve(arti.clone(), hostname.clone());
        }

        // Make a random IPv6 address in the range fec0::/10.
        // This range is the deprecated "site-local address" range, but it should still
        // be fine to use; it's just a convenient reserved block that's definitely not globally
        // routable.

        // It is not necessary to check for an address space exhaustion,
        // as other operating system limits are more likely to be hit,
        // before self.map_v6.len() exceeds 2^118.

        let mut ret = Ipv6Address([0xfe, 0xc0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]);
        loop {
            rand::thread_rng().fill_bytes(&mut ret.0[2..]);
            if !self.map_v6.contains_left(&ret) {
                break;
            }
        }
        assert!(!self.map_v6.contains_left(&ret));

        let _ = self.map_v6.insert_no_overwrite(ret, hostname);
        Some(ret)
    }

    /// Resolve a hostname to ResolverResult, using a TorClient, without interacting with the cache
    #[cfg(feature = "udp-tunnel")]
    async fn uncached_resolve<S: TunnelScaffolding>(
        arti: &TorClient<OnionTunnelArtiRuntime<S>>,
        isolation: &IsolationToken,
        hostname: &str,
    ) -> ResolverResult {
        trace!("Asynchronously resolving {:?}", hostname);
        let mut stream_prefs = StreamPrefs::new();
        // TODO: Restrict the country code for DNS?
        stream_prefs.set_isolation(*isolation);
        let result = arti.resolve_with_prefs(hostname, &stream_prefs).await;
        trace!("Asynchronously resolved {:?} -> {:?}", hostname, result);
        result
    }

    /// Begin asynchronously looking up the real IP addresses for a hostname
    #[cfg(feature = "udp-tunnel")]
    fn begin_resolve<S: TunnelScaffolding>(
        &self,
        arti: TorClient<OnionTunnelArtiRuntime<S>>,
        hostname: String,
    ) {
        let sender = self.resolver_send.clone();
        let isolation = self.isolation_token;
        tokio::spawn(async move {
            let result = Self::uncached_resolve(&arti, &isolation, &hostname).await;
            let _ = sender.send((hostname, result)).await;
        });
    }

    /// Return the resolver result for a particular hostname, if it has finished
    #[cfg(feature = "udp-tunnel")]
    fn resolve_result(&mut self, hostname: &str) -> Option<&ResolverResult> {
        while let Ok((hostname, result)) = self.resolver_recv.try_recv() {
            self.resolver_map.insert(hostname, result);
        }
        self.resolver_map.get(hostname)
    }

    fn lookup_address(&self, addr: &IpAddress) -> Option<String> {
        match addr {
            IpAddress::Ipv4(a) => self.map_v4.get_by_left(a).cloned(),
            IpAddress::Ipv6(a) => self.map_v6.get_by_left(a).cloned(),
        }
    }
}

pub(crate) struct DnsCookies {
    /// Per-isolation-key isolated store of cookies.
    cookies: HashMap<Option<u64>, IsolatedCookies>,
}

impl DnsCookies {
    fn new() -> Self {
        Self {
            cookies: HashMap::new(),
        }
    }

    fn make_for_v4<S: TunnelScaffolding>(
        &mut self,
        arti: Option<&TorClient<OnionTunnelArtiRuntime<S>>>,
        isolation_key: Option<u64>,
        hostname: String,
    ) -> Option<Ipv4Address> {
        self.cookies
            .entry(isolation_key)
            .or_insert_with(IsolatedCookies::new)
            .make_for_v4(arti, hostname)
    }

    fn make_for_v6<S: TunnelScaffolding>(
        &mut self,
        arti: Option<&TorClient<OnionTunnelArtiRuntime<S>>>,
        isolation_key: Option<u64>,
        hostname: String,
    ) -> Option<Ipv6Address> {
        self.cookies
            .entry(isolation_key)
            .or_insert_with(IsolatedCookies::new)
            .make_for_v6(arti, hostname)
    }

    pub fn get(&self, isolation_key: Option<u64>, addr: &IpAddress) -> Option<String> {
        self.cookies
            .get(&isolation_key)
            .and_then(|x| x.lookup_address(addr))
    }

    /// Return the resolver result for a particular hostname, if it has finished
    #[cfg(feature = "udp-tunnel")]
    fn resolve_result(
        &mut self,
        isolation_key: Option<u64>,
        hostname: &str,
    ) -> Option<&ResolverResult> {
        self.cookies
            .get_mut(&isolation_key)
            .and_then(|cookies| cookies.resolve_result(hostname))
    }

    /// Return an appropriate Arti IsolationToken corresponding to a DNS cookie isolation key
    ///
    /// Allocates a new token if necessary, remembering the mapping.
    #[cfg(feature = "udp-tunnel")]
    fn isolation_token(&mut self, isolation_key: Option<u64>) -> &IsolationToken {
        &self
            .cookies
            .entry(isolation_key)
            .or_insert_with(IsolatedCookies::new)
            .isolation_token
    }

    /// Look up the most recent resolver output for a hostname, or wait for one resolution now
    ///
    /// If the resolver has already completed at least once for this hostname and set of isolation
    /// cookies, we return that cached result. Otherwise a single attempt is performed before
    /// returning, and the result of that attempt stored if it completes.
    ///
    #[cfg(feature = "udp-tunnel")]
    async fn cached_resolve<S: TunnelScaffolding>(
        locked: &LockedDnsCookies,
        arti: &TorClient<OnionTunnelArtiRuntime<S>>,
        isolation_key: Option<u64>,
        hostname: &str,
    ) -> ResolverResult {
        // Take the lock once to check for a cached result, and get the correct Arti stream isolation token
        let (cached, isolation_token) = {
            #[allow(clippy::expect_used)]
            let mut cookies = locked.lock().expect("dns shared data lock");
            (
                cookies.resolve_result(isolation_key, hostname).cloned(),
                cookies.isolation_token(isolation_key).to_owned(),
            )
        };
        match cached {
            Some(cached) => cached,
            None => {
                // Wait for resolve over Tor
                let result =
                    IsolatedCookies::uncached_resolve(arti, &isolation_token, hostname).await;
                // Lock again to store
                #[allow(clippy::expect_used)]
                locked
                    .lock()
                    .expect("dns shared data lock")
                    .cookies
                    .entry(isolation_key)
                    .or_insert_with(IsolatedCookies::new)
                    .resolver_map
                    .insert(hostname.to_string(), result.clone());
                result
            }
        }
    }

    /// Find a real IP address corresponding to an address that might be fake (isolated)
    ///
    /// If the provided address does not look fake, it's returned unmodified.
    /// Always returns an address from the same family as the given fake address.
    ///
    #[cfg(feature = "udp-tunnel")]
    pub async fn get_resolved<S: TunnelScaffolding>(
        locked: &LockedDnsCookies,
        arti: &TorClient<OnionTunnelArtiRuntime<S>>,
        isolation_key: Option<u64>,
        addr: &IpAddress,
    ) -> Option<IpAddress> {
        if IsolatedCookies::ip_in_randomized_range(addr) {
            #[allow(clippy::expect_used)]
            let hostname = locked
                .lock()
                .expect("dns shared data lock")
                .get(isolation_key, addr);
            match hostname {
                None => None,
                Some(hostname) => {
                    match Self::cached_resolve(locked, arti, isolation_key, &hostname).await {
                        Err(_e) => None,
                        Ok(ip_addrs) => {
                            for answer in ip_addrs {
                                match (addr, answer) {
                                    (IpAddress::Ipv4(_), IpAddr::V4(_)) => {
                                        return Some(answer.into())
                                    }
                                    (IpAddress::Ipv6(_), IpAddr::V6(_)) => {
                                        return Some(answer.into())
                                    }
                                    _ => (),
                                }
                            }
                            None
                        }
                    }
                }
            }
        } else {
            Some(*addr)
        }
    }
}

pub struct DnsManager<S: TunnelScaffolding> {
    listener_udp_v4: UdpSocket,
    listener_udp_v6: UdpSocket,
    cookies: LockedDnsCookies,
    arti: TorClient<OnionTunnelArtiRuntime<S>>,
    scaffolding: Arc<S>,
    #[cfg(feature = "udp-tunnel")]
    udp_prebuild: mpsc::Sender<UdpPrebuildRequest>,
}

impl<S> DnsManager<S>
where
    S: TunnelScaffolding,
{
    /// The IPv4 address the DNS server listens on.
    ///
    /// Uses the 169.254.0.0/16 link-local block.
    const V4_ADDRESS: Ipv4Addr = Ipv4Addr::new(169, 254, 42, 53);
    /// The IPv6 address the DNS server listens on.
    ///
    /// Uses the fe80::/10 link-local block.
    const V6_ADDRESS: Ipv6Addr = Ipv6Addr::new(0xfe80, 0, 0, 0, 0, 0, 0, 0x53);

    pub fn new<D>(tunnel: &OnionTunnel<S, D>) -> TunnelResult<Self> {
        Ok(Self {
            listener_udp_v4: Self::new_udp_listener(tunnel, Self::V4_ADDRESS.into())?,
            listener_udp_v6: Self::new_udp_listener(tunnel, Self::V6_ADDRESS.into())?,
            cookies: Arc::new(Mutex::new(DnsCookies::new())),
            arti: tunnel.arti.isolated_client(),
            scaffolding: tunnel.scaffolding.clone(),
            #[cfg(feature = "udp-tunnel")]
            udp_prebuild: tunnel.udp_tunnel.prebuild_sender().clone(),
        })
    }

    fn new_udp_listener<D>(
        tunnel: &OnionTunnel<S, D>,
        bind_addr: IpAddress,
    ) -> TunnelResult<UdpSocket> {
        let rx_buffer = UdpSocketBuffer::new(vec![UdpPacketMetadata::EMPTY], vec![0; 4096]);
        let tx_buffer = UdpSocketBuffer::new(vec![UdpPacketMetadata::EMPTY], vec![0; 4096]);

        let socket = udp::Socket::new(rx_buffer, tx_buffer);

        let mut listener = UdpSocket::new(tunnel, socket);
        #[allow(clippy::expect_used)]
        listener
            .bind(bind_addr, 53)
            .expect("bind error when creating DNS listener?");

        Ok(listener)
    }

    pub(crate) fn cookies(&self) -> Arc<Mutex<DnsCookies>> {
        self.cookies.clone()
    }

    fn make_answer(
        query: &dns_message_parser::Dns,
        rcode: RCode,
        answers: Vec<RR>,
    ) -> dns_message_parser::Dns {
        let flags = dns_message_parser::Flags {
            qr: true,
            opcode: dns_message_parser::Opcode::Query,
            aa: false,
            tc: false,
            rd: true,
            ra: true,
            ad: false,
            cd: false,
            rcode,
        };
        // NOTE: We can't really add authorities or additionals since we don't know
        //       what they are!
        dns_message_parser::Dns {
            id: query.id,
            flags,
            questions: query.questions.clone(),
            answers,
            authorities: vec![],
            additionals: vec![],
        }
    }

    fn build_answer(
        &self,
        query: &dns_message_parser::Dns,
        from: IpEndpoint,
    ) -> Option<dns_message_parser::Dns> {
        let dest = if matches!(from.addr, IpAddress::Ipv4(..)) {
            IpEndpoint::new(Self::V4_ADDRESS.into(), 53)
        } else {
            IpEndpoint::new(Self::V6_ADDRESS.into(), 53)
        };

        // We try and get the isolation key, in order to isolate DNS per-app.
        // If we can't, we store it in the fallback key, which the proxying code will check if it
        // can't find the isolation key.
        // This does kind of ruin the feature slightly, but it doesn't work properly on some
        // Android platforms without this. Sigh.
        let isolation_key = match self.scaffolding.isolate(from, dest, 17 /* UDP */) {
            Ok(k) => Some(k),
            Err(e) => {
                trace!(
                    "DNS query from {} failed due to isolate() call failure: {}",
                    from,
                    e
                );
                None
                /*
                NOTE(eta): It would be nice if we could do this...
                return Some(Self::make_answer(query, RCode::ServFail, vec![]));
                 */
            }
        };

        // For UDP tunnel pre-building, look up what the associated country code would be
        #[cfg(feature = "udp-tunnel")]
        let country_code = isolation_key.and_then(|key| self.scaffolding.locate(from, dest, key));

        let mut answers = vec![];

        for question in query.questions.iter() {
            let name = question.domain_name.to_string();
            let rr = match &question.q_type {
                &QType::A => {
                    #[allow(clippy::expect_used)]
                    let mut cookies = self.cookies.lock().expect("cookie poisoned");
                    let cookie = match cookies.make_for_v4(Some(&self.arti), isolation_key, name) {
                        Some(cookie) => cookie,
                        // Return a `Refused` in case of an address exhaustion.
                        None => return Some(Self::make_answer(query, RCode::Refused, vec![])),
                    };

                    #[cfg(feature = "udp-tunnel")]
                    if let Some(isolation_key) = isolation_key {
                        let _ = self.udp_prebuild.try_send(UdpPrebuildRequest {
                            isolation_key,
                            country_code,
                            address_family: turn_tcp_client::proto::REQUESTED_FAMILY_IPV4,
                        });
                    }
                    RR::A(dns_message_parser::rr::A {
                        domain_name: question.domain_name.clone(),
                        ttl: 3600,
                        ipv4_addr: Ipv4Addr::from(cookie),
                    })
                }
                &QType::AAAA => {
                    #[allow(clippy::expect_used)]
                    let mut cookies = self.cookies.lock().expect("cookie poisoned");
                    let cookie = match cookies.make_for_v6(Some(&self.arti), isolation_key, name) {
                        Some(cookie) => cookie,
                        // Return a `Refused` in case of an address exhaustion.
                        None => return Some(Self::make_answer(query, RCode::Refused, vec![])),
                    };
                    #[cfg(feature = "udp-tunnel")]
                    if let Some(isolation_key) = isolation_key {
                        let _ = self.udp_prebuild.try_send(UdpPrebuildRequest {
                            isolation_key,
                            country_code,
                            address_family: turn_tcp_client::proto::REQUESTED_FAMILY_IPV6,
                        });
                    }
                    RR::AAAA(dns_message_parser::rr::AAAA {
                        domain_name: question.domain_name.clone(),
                        ttl: 3600,
                        ipv6_addr: Ipv6Addr::from(cookie),
                    })
                }
                &QType::HTTPS | &QType::SVCB => {
                    warn!(
                        "Received DNS query of type {}. The Tor network doesn't support it yet.",
                        question.q_type,
                    );
                    // return "not implemented".
                    return Some(Self::make_answer(query, RCode::NotImp, vec![]));
                }
                // TODO(eta): implement PTR
                x => {
                    warn!(
                        "Received DNS query of type {}, which we don't support yet!",
                        x
                    );
                    // return "not implemented" -- at least it's better than nothing!
                    return Some(Self::make_answer(query, RCode::NotImp, vec![]));
                }
            };
            answers.push(rr);
        }

        Some(Self::make_answer(query, RCode::NoError, answers))
    }

    fn decode(&self, payload: Bytes, from: IpEndpoint) -> Option<dns_message_parser::Dns> {
        let ret = dns_message_parser::Dns::decode(payload);
        match ret {
            Ok(p) => {
                trace!("DNS question from {}: {}", from, p);
                let ret = self.build_answer(&p, from);
                if let Some(ref r) = ret {
                    trace!("DNS answer to {}: {}", from, r);
                } else {
                    trace!("no DNS answer for {}", from);
                }
                ret
            }
            Err(e) => {
                warn!("Unable to parse DNS request: {}", e);
                None
            }
        }
    }

    pub async fn start(&mut self) {
        info!("Starting DNS manager");

        loop {
            tokio::select! {
                r = self.listener_udp_v4.next() => match r {
                    Some((payload, endpoint)) => {
                        if let Some(reply) = self.decode(payload, endpoint) {
                            match reply.encode() {
                                Ok(v) => {
                                    if let Err(e) = self.listener_udp_v4.send(&v, endpoint) {
                                        warn!("Failed to send DNS reply: {e:?}");
                                    }
                                },
                                Err(e) => warn!("Failed to encode DNS reply: {e}")
                            }
                        }
                    }
                    None => break,
                },
                r = self.listener_udp_v6.next() => match r {
                    Some((payload, endpoint)) => {
                        if let Some(reply) = self.decode(payload, endpoint) {
                            match reply.encode() {
                                Ok(v) => {
                                    if let Err(e) = self.listener_udp_v6.send(&v, endpoint) {
                                        warn!("Failed to send DNS reply: {e:?}");
                                    }
                                },
                                Err(e) => warn!("Failed to encode DNS reply: {e}")
                            }
                        }
                    }
                    None => break,
                }
            }
        }
    }
}

#[cfg(test)]
mod test {
    use crate::dns::DnsCookies;
    use smoltcp::wire::{IpAddress, Ipv4Address};

    use super::IsolatedCookies;

    #[test]
    fn dns_cookie_basics() {
        let mut cookies = DnsCookies::new();

        let hostname_4: String = "test.hostname".into();
        let hostname_6: String = "lol.arpa".into();

        let v4 = IpAddress::Ipv4(
            cookies
                .make_for_v4::<()>(None, Some(0), hostname_4.clone())
                .unwrap(),
        );
        let v6 = IpAddress::Ipv6(
            cookies
                .make_for_v6::<()>(None, Some(0), hostname_6.clone())
                .unwrap(),
        );

        assert_eq!(cookies.get(Some(0), &v4), Some(hostname_4));
        assert_eq!(cookies.get(Some(0), &v6), Some(hostname_6));
        assert_eq!(
            cookies.get(Some(0), &IpAddress::Ipv4(Ipv4Address([1, 2, 3, 4]))),
            None
        );
    }

    #[test]
    fn dns_cookie_addresses_stay_the_same() {
        let mut cookies = DnsCookies::new();

        let hostname_4: String = "test.hostname".into();
        let hostname_6: String = "lol.arpa".into();

        let v4 = IpAddress::Ipv4(
            cookies
                .make_for_v4::<()>(None, Some(0), hostname_4.clone())
                .unwrap(),
        );
        let v6 = IpAddress::Ipv6(
            cookies
                .make_for_v6::<()>(None, Some(0), hostname_6.clone())
                .unwrap(),
        );
        let same_v4 = IpAddress::Ipv4(
            cookies
                .make_for_v4::<()>(None, Some(0), hostname_4)
                .unwrap(),
        );
        let same_v6 = IpAddress::Ipv6(
            cookies
                .make_for_v6::<()>(None, Some(0), hostname_6)
                .unwrap(),
        );
        assert_eq!(v4, same_v4);
        assert_eq!(v6, same_v6);
    }

    #[test]
    fn dns_cookies_are_isolated() {
        let mut cookies = DnsCookies::new();

        let hostname_4: String = "test.hostname".into();
        let hostname_6: String = "lol.arpa".into();

        let v4_0 = IpAddress::Ipv4(
            cookies
                .make_for_v4::<()>(None, Some(0), hostname_4.clone())
                .unwrap(),
        );
        let v6_0 = IpAddress::Ipv6(
            cookies
                .make_for_v6::<()>(None, Some(0), hostname_6.clone())
                .unwrap(),
        );

        assert_eq!(cookies.get(Some(0), &v4_0), Some(hostname_4));
        assert_eq!(cookies.get(Some(1), &v4_0), None);
        assert_eq!(cookies.get(Some(0), &v6_0), Some(hostname_6));
        assert_eq!(cookies.get(Some(1), &v6_0), None);
    }

    #[test]
    fn dns_cookies_are_distinct_v4() {
        let mut cookies = DnsCookies::new();
        let addresses = (0..64)
            .map(|x| {
                IpAddress::Ipv4(
                    cookies
                        .make_for_v4::<()>(None, Some(0), format!("random-hostname{x}.arpa"))
                        .unwrap(),
                )
            })
            .collect::<Vec<_>>();

        // Yes, this is silly and inefficient. No, I don't care much.
        for (ia, a) in addresses.iter().enumerate() {
            for (ib, b) in addresses.iter().enumerate() {
                if ia != ib {
                    assert_ne!(a, b);
                }
            }
        }
    }

    #[test]
    fn dns_cookies_are_distinct_v6() {
        let mut cookies = DnsCookies::new();
        let addresses = (0..64)
            .map(|x| {
                IpAddress::Ipv6(
                    cookies
                        .make_for_v6::<()>(None, Some(0), format!("random-hostname{x}.arpa"))
                        .unwrap(),
                )
            })
            .collect::<Vec<_>>();

        // Yes, this is silly and inefficient. No, I don't care much.
        for (ia, a) in addresses.iter().enumerate() {
            for (ib, b) in addresses.iter().enumerate() {
                if ia != ib {
                    assert_ne!(a, b);
                }
            }
        }
    }

    #[test]
    fn ipv4_address_full() {
        let mut cookies = IsolatedCookies::new();
        // Those magic numbers are 10.0.0.1 to 10.255.255.255.
        // This is slow.
        for ip in 167772161..184549375u32 {
            let a = ((ip >> 24) & 0xFF) as u8;
            let b = ((ip >> 16) & 0xFF) as u8;
            let c = ((ip >> 8) & 0xFF) as u8;
            let d = (ip & 0xFF) as u8;

            let addr = Ipv4Address([a, b, c, d]);

            // We have to insert manually, because using the random make_for_v4
            // could take an eternity.
            cookies.map_v4.insert(addr, format!("{ip}.arpa"));
        }

        // Get a failure.
        assert_eq!(cookies.make_for_v4::<()>(None, "foo.arpa".into()), None);
    }
}
