//! UDP Tunneling with TURN over Tor
//!
//! This builds on the TURN protocol utilities from the `webrtc` crate and the
//! TURN-over-TCP client from the `turn-tcp-client` crate. Here we add logic
//! for managing multiple allocations and interoperating with onion-tunnel
//! features, presenting a high-level interface for sending and
//! receiving tunneled datagrams.
//!

mod allocation;
mod device;
mod send;
mod tunnel;

use std::net::SocketAddr;

pub(crate) use device::forward_tunnel_to_device;
pub(crate) use send::{UdpSendContext, UdpTrySendResult};
pub(crate) use tunnel::{UdpPrebuildRequest, UdpTunnel};

/// Owned UDP datagram, received by the [`tunnel::UdpTunnel`]
#[derive(Debug, Clone, PartialEq, Eq)]
pub(crate) struct UdpData {
    pub(crate) source: SocketAddr,
    pub(crate) dest: SocketAddr,
    pub(crate) data: Vec<u8>,
}
