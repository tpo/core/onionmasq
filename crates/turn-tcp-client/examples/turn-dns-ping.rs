use arti_client::{IntoTorAddr, TorClient, TorClientConfig};
use clap::Parser;
use dns_message_parser::question::{QClass, QType, Question};
use dns_message_parser::{Dns, Flags, Opcode, RCode};
use futures::io::{AsyncRead, AsyncReadExt, AsyncWrite};
use std::collections::HashMap;
use std::time::Instant;
use tokio::net::lookup_host;
use tokio::sync::mpsc;
use tokio::time::{sleep, Duration};
use tor_rtcompat::{NetStreamProvider, PreferredRuntime};
use turn_tcp_client::proto::REQUESTED_FAMILY_IPV4;
use turn_tcp_client::{
    allocation::{TurnAllocation, TurnAllocationRead},
    auth::TurnAuth,
    proto::{TurnTcpRead, TurnTcpWrite},
    DEFAULT_PORT,
};

#[derive(Parser)]
#[command(version, about, long_about = None)]
struct Cli {
    turn_host: String,
    #[arg(short, long, default_value_t = DEFAULT_PORT)]
    turn_port: u16,
    #[arg(short = 'T', long, action)]
    use_tor: bool,
    #[arg(short = 's', long)]
    turn_secret: Option<String>,
    #[arg(short, long, default_value = "8.8.8.8")]
    dns_host: String,
    #[arg(long, default_value_t = 53)]
    dns_port: u16,
    #[arg(long, default_value = "example.com")]
    dns_question: String,
    #[arg(short, long, default_value_t = 4.0)]
    interval: f32,
}

#[tokio::main]
async fn main() {
    env_logger::init();
    let cli = Cli::parse();
    let turn_auth = match cli.turn_secret {
        None => TurnAuth::None,
        Some(secret) => TurnAuth::Secret(secret),
    };

    let dns_addr = lookup_host((cli.dns_host, cli.dns_port))
        .await
        .unwrap()
        .next()
        .unwrap();
    println!(
        "Will be using DNS server {:?}, looking up {:?} every {:?} sec",
        dns_addr, cli.dns_question, cli.interval
    );

    let (reader, writer): (
        Box<dyn AsyncRead + Send + Unpin>,
        Box<dyn AsyncWrite + Send + Unpin>,
    ) = if cli.use_tor {
        let tor_config = TorClientConfig::default();

        println!("Tor bootstrapping...");
        let tor = TorClient::create_bootstrapped(tor_config).await.unwrap();

        println!("Connecting TURN client over Tor...");
        let (reader, writer) = tor
            .connect((cli.turn_host, cli.turn_port).into_tor_addr().unwrap())
            .await
            .unwrap()
            .split();
        (Box::new(reader), Box::new(writer))
    } else {
        let addr = lookup_host((cli.turn_host, cli.turn_port))
            .await
            .unwrap()
            .next()
            .unwrap();
        println!("Connecting TURN client directly to {addr:?}...");
        let (reader, writer) = PreferredRuntime::current()
            .unwrap()
            .connect(&addr)
            .await
            .unwrap()
            .split();
        (Box::new(reader), Box::new(writer))
    };

    let mut reader = TurnTcpRead::new(reader);
    let mut writer = TurnTcpWrite::new(writer);

    println!("Allocating...");
    let mut alloc = TurnAllocation::new(
        &mut reader,
        &mut writer,
        &turn_auth,
        REQUESTED_FAMILY_IPV4,
        Instant::now(),
    )
    .await
    .unwrap();
    println!(
        "Allocation complete, relayed address is {:?}",
        alloc.relayed_address()
    );

    let (timestamp_sender, mut timestamp_receiver) = mpsc::channel(16);
    let (signal_sender, mut signal_receiver) = mpsc::channel(16);

    tokio::spawn(async move {
        let mut timestamps: HashMap<u16, Instant> = Default::default();
        loop {
            match TurnAllocation::read(&mut reader)
                .await
                .expect("TURN reader error")
            {
                TurnAllocationRead::Signal(s) => {
                    signal_sender.send(s).await.unwrap();
                }
                TurnAllocationRead::ChannelData(channel, data) => {
                    while let Ok((id, timestamp)) = timestamp_receiver.try_recv() {
                        timestamps.insert(id, timestamp);
                    }
                    match Dns::decode(data.into()) {
                        Err(e) => {
                            println!("Error decoding response from {:?}, {:?}", channel, e);
                        }
                        Ok(dns) => match timestamps.get(&dns.id) {
                            None => {
                                println!(
                                    "Response from {:?} with unexpected id, {:?}",
                                    channel, dns
                                );
                            }
                            Some(timestamp) => {
                                let latency = Instant::now() - *timestamp;
                                println!(
                                    "<- {:?} msec response latency, {:?}\n{:?}",
                                    latency.as_millis(),
                                    channel,
                                    dns
                                );
                            }
                        },
                    }
                }
            }
        }
    });

    let mut query = Dns {
        id: 0,
        flags: Flags {
            qr: false,
            opcode: Opcode::Query,
            aa: false,
            tc: false,
            rd: false,
            ra: false,
            ad: false,
            cd: false,
            rcode: RCode::NoError,
        },
        questions: vec![Question {
            domain_name: cli.dns_question.parse().unwrap(),
            q_class: QClass::IN,
            q_type: QType::A,
        }],
        additionals: vec![],
        answers: vec![],
        authorities: vec![],
    };

    loop {
        while let Ok(signal) = signal_receiver.try_recv() {
            println!("Handling reader signal");
            alloc.handle_signal(signal, Instant::now()).unwrap();
        }

        // Timestamp each query right before we send it
        query.id = query.id.wrapping_add(1);
        timestamp_sender
            .send((query.id, Instant::now()))
            .await
            .unwrap();

        let status = alloc
            .send(
                &mut writer,
                Instant::now(),
                dns_addr,
                &query.encode().unwrap(),
            )
            .await
            .unwrap();
        println!(
            "-> Sending to peer {:?} {:?}\n{:?}",
            dns_addr, status.info.number, query
        );
        sleep(Duration::from_millis((cli.interval * 1000.0) as u64)).await;
    }
}
