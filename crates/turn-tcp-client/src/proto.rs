//! Additional protocol support for TURN-over-TCP

use crate::auth::{TurnAuth, TurnSession};
use crate::error::Error;
use futures::io::{AsyncRead, AsyncReadExt, AsyncWrite, AsyncWriteExt};
use std::net::SocketAddr;
use stun::attributes::ATTR_NONCE;
use stun::error_code::{ErrorCodeAttribute, CODE_UNAUTHORIZED};
use stun::message::{
    Getter, Message, MessageType, Setter, CLASS_ERROR_RESPONSE, CLASS_REQUEST,
    CLASS_SUCCESS_RESPONSE, METHOD_CHANNEL_BIND,
};
use stun::textattrs::TextAttribute;
use turn::proto::channum::ChannelNumber;
use turn::proto::peeraddr::PeerAddress;
use turn::proto::reqtrans::RequestedTransport;
use turn::proto::PROTO_UDP;

// Re-export of RequestedAddressFamily definitions (RFC 6156 Section 4.1.1, via the `turn` crate)
pub use turn::proto::reqfamily::{
    RequestedAddressFamily, REQUESTED_FAMILY_IPV4, REQUESTED_FAMILY_IPV6,
};

/// Owned message type, either a STUN Message or a TURN ChannelData.
///
/// Messages are stored as a single raw Vec, to match the underlying type used
/// by the `stun` crate. ChannelData are stored with their datagram contents
/// in a separate Vec from the associated header data, so the datagram may be
/// routed by upper layers without additional copies made.
pub enum TurnMessageOwned {
    Stun(Message),
    ChannelData(ChannelNumber, Vec<u8>),
}

const CHANNEL_DATA_HEADER_SIZE: usize = 4;
const CHANNEL_DATA_ALIGN: usize = 4;

/// Writer for TURN-over-TCP protocol, implemented over any `AsyncWrite`
pub struct TurnTcpWrite<T>(T);

impl<T: AsyncWrite + Unpin> TurnTcpWrite<T> {
    /// Wrap an [`AsyncWrite`] in a layer which can write only properly-framed TURN messages
    pub fn new(inner: T) -> Self {
        Self(inner)
    }

    /// Reference the wrapped item
    pub fn inner(&self) -> &T {
        &self.0
    }

    /// Mutably reference the wrapped item
    pub fn inner_mut(&mut self) -> &mut T {
        &mut self.0
    }

    /// Send and flush one STUN message
    pub async fn write_stun(&mut self, msg: &Message) -> Result<(), Error> {
        log::trace!("Outgoing STUN message, {:?}", msg);
        self.0.write_all(&msg.raw).await?;
        self.0.flush().await?;
        Ok(())
    }

    /// Send ChannelData
    pub async fn write_channel_data(
        &mut self,
        number: ChannelNumber,
        data: &[u8],
    ) -> Result<(), Error> {
        let data_len: u16 = data.len().try_into().map_err(|_| Error::DatagramTooLarge)?;
        self.0.write_all(&number.0.to_be_bytes()).await?;
        self.0.write_all(&data_len.to_be_bytes()).await?;
        self.0.write_all(data).await?;
        self.write_padding_for_channel_data(data_len).await?;
        self.0.flush().await?;
        Ok(())
    }

    /// Write up to 3 zero bytes to bring a `ChannelData` message to the next alignment boundary
    async fn write_padding_for_channel_data(&mut self, data_len: u16) -> Result<(), Error> {
        let misalignment = (data_len as usize) % CHANNEL_DATA_ALIGN;
        if misalignment != 0 {
            self.0
                .write_all(&[0u8; CHANNEL_DATA_ALIGN][misalignment..CHANNEL_DATA_ALIGN])
                .await?;
        }
        Ok(())
    }
}

/// Reader for TURN-over-TCP, implemented over any `AsyncRead`
pub struct TurnTcpRead<T>(T);

impl<T: AsyncRead + Unpin> TurnTcpRead<T> {
    /// Wrap an [`AsyncRead`] in a layer which can only reads properly-framed TURN messages
    ///
    /// This wrapper assumes the underlying device is already buffered, preferring
    /// to make small reads that need no additional copying rather than making larger
    /// reads and then copying out portions as needed.
    ///
    pub fn new(inner: T) -> Self {
        Self(inner)
    }

    /// Reference the wrapped item
    pub fn inner(&self) -> &T {
        &self.0
    }

    /// Mutably reference the wrapped item
    pub fn inner_mut(&mut self) -> &mut T {
        &mut self.0
    }

    /// Read the next incoming frame into a `TurnMessageOwned`
    ///
    /// This examines the first 4 bytes to discern between a STUN message
    /// and a TURN ChannelData message. Allocates at most slightly over 64kB.
    ///
    pub async fn read(&mut self) -> Result<TurnMessageOwned, Error> {
        let mut header = [0u8; CHANNEL_DATA_HEADER_SIZE];
        self.0.read_exact(&mut header).await?;

        let channel_num = ChannelNumber(u16::from_be_bytes([header[0], header[1]]));
        let length_field = u16::from_be_bytes([header[2], header[3]]) as usize;
        let mut buffer = Vec::new();

        if channel_num.valid() {
            // This is TURN ChannelData
            let padded_length_field = length_field.next_multiple_of(CHANNEL_DATA_ALIGN);
            // Prefer to avoid https://rust-lang.github.io/rust-clippy/master/index.html#/read_zero_byte_vec
            if padded_length_field > 0 {
                // Note that this allocates at most 64kB
                buffer.resize(padded_length_field, 0);
                self.0.read_exact(&mut buffer).await?;
            }
            buffer.resize(length_field, 0);
            Ok(TurnMessageOwned::ChannelData(channel_num, buffer))
        } else {
            // This is a STUN Message
            const STUN_HEADER_LEN: usize = 20;
            let total_length = length_field + STUN_HEADER_LEN;
            // Note that this allocates at most slightly over 64kB
            buffer.resize(total_length, 0);
            buffer[..header.len()].copy_from_slice(&header);
            self.0.read_exact(&mut buffer[header.len()..]).await?;
            let mut message = Message {
                raw: buffer,
                ..Default::default()
            };
            message.decode()?;
            Ok(TurnMessageOwned::Stun(message))
        }
    }
}

/// Wait for a single STUN transaction to finish
///
/// This is for cases where we are issuing one transaction per connection,
/// without any other overlapping traffic. Intended for connection setup.
///
/// Requires a mutable reference for both the TurnTcpRead and TurnTcpWrite
/// for the duration of the transation. Waits until a matching reply arrives
/// or an I/O error occurs. Unexpected incoming datagrams are ignored.
///
pub async fn transaction<W: AsyncWrite + Unpin, R: AsyncRead + Unpin>(
    writer: &mut TurnTcpWrite<W>,
    reader: &mut TurnTcpRead<R>,
    msg: &Message,
) -> Result<Message, Error> {
    log::trace!("STUN transaction -> {:?}", msg);
    writer.write_stun(msg).await?;
    loop {
        if let TurnMessageOwned::Stun(incoming) = reader.read().await? {
            if incoming.transaction_id == msg.transaction_id {
                log::trace!("STUN transaction reply <- {:?}", incoming);
                return Ok(incoming);
            }
        }
    }
}

/// Issue a TURN "allocate" transaction, optionally with authentication
///
/// This does wait for at least one full round-trip, confirming that the
/// allocation exists on the server before we allow normal use of the TURN
/// client.
///
/// It would have been nice to avoid this delay and assume the allocation
/// would exist on the server by the time it processes subsequent incoming
/// messages. Unfortunately this doesn't work on the common `coturn` server
/// implementation due to its internally asynchronous processing of allocations.
///
/// It's better for application behavior if we delay the first outgoing packets
/// until the Allocation is confirmed, rather than risking them being dropped
/// by the remote server.
///
/// Returns the allocation reply as a `Message`, along with the auth session
/// if one was necessary.
///
pub async fn allocate<W: AsyncWrite + Unpin, R: AsyncRead + Unpin>(
    writer: &mut TurnTcpWrite<W>,
    reader: &mut TurnTcpRead<R>,
    auth: &TurnAuth,
    addr_family: RequestedAddressFamily,
) -> Result<(Message, Option<TurnSession>), Error> {
    let proto_udp = RequestedTransport {
        protocol: PROTO_UDP,
    };

    // Unauthenticated request, succeeds only if the server has auth disabled.
    // We need the response to determine the auth nonce and realm.
    let mut message = Message::new();
    message.new_transaction_id()?;
    message.set_type(turn::proto::allocate_request());
    message.write_header();
    proto_udp.add_to(&mut message)?;
    addr_family.add_to(&mut message)?;

    let reply = transaction(writer, reader, &message).await?;

    let mut error_code = ErrorCodeAttribute::default();
    let mut nonce = TextAttribute {
        attr: ATTR_NONCE,
        ..Default::default()
    };

    if reply.typ.class == CLASS_SUCCESS_RESPONSE {
        // Succeeded without authentication
        Ok((reply, None))
    } else if reply.transaction_id == message.transaction_id
        && reply.typ.class == CLASS_ERROR_RESPONSE
        && error_code.get_from(&reply).is_ok()
        && error_code.code == CODE_UNAUTHORIZED
        && nonce.get_from(&reply).is_ok()
    {
        // Start an authenticated session using the provided nonce value
        let session = TurnSession::new(auth, &reply)?;

        // Authenticated retry
        message.new_transaction_id()?;
        session.add_to(&mut message)?;
        let reply = transaction(writer, reader, &message).await?;
        if reply.typ.class == CLASS_SUCCESS_RESPONSE {
            Ok((reply, Some(session)))
        } else {
            Err(unsuccessful_transaction_error(&reply))
        }
    } else {
        Err(unsuccessful_transaction_error(&reply))
    }
}

/// Issue a TURN allocation refresh request
///
/// This does not wait for a reply.
///
pub async fn refresh<W: AsyncWrite + Unpin>(
    writer: &mut TurnTcpWrite<W>,
    session: &Option<TurnSession>,
) -> Result<(), Error> {
    let mut message = Message::new();
    message.new_transaction_id()?;
    message.set_type(turn::proto::refresh_request());
    message.write_header();
    if let Some(session) = session {
        session.add_to(&mut message)?;
    }
    writer.write_stun(&message).await?;
    Ok(())
}

/// Issue a TURN binding request
///
/// This does not wait for a reply. We assume the binding is usable immediately,
/// since TURN-over-TCP will not experience reordered or dropped messages.
///
pub async fn bind<W: AsyncWrite + Unpin>(
    writer: &mut TurnTcpWrite<W>,
    session: &Option<TurnSession>,
    channel: ChannelNumber,
    peer: SocketAddr,
) -> Result<(), Error> {
    let mut message = Message::new();
    let peer = PeerAddress {
        ip: peer.ip(),
        port: peer.port(),
    };
    message.new_transaction_id()?;
    message.set_type(MessageType::new(METHOD_CHANNEL_BIND, CLASS_REQUEST));
    message.write_header();
    channel.add_to(&mut message)?;
    peer.add_to(&mut message)?;
    if let Some(session) = session {
        session.add_to(&mut message)?;
    }
    writer.write_stun(&message).await?;
    Ok(())
}

/// Return an [`Error`] for any unexpected or failed STUN reply [`Message`]
///
/// This is used internally by [`transaction`], and callers may want the
/// same behavior when processing asynchronous error replies.
///
pub fn unsuccessful_transaction_error(reply: &Message) -> Error {
    let mut error_code = ErrorCodeAttribute::default();
    if error_code.get_from(reply).is_ok() {
        log::warn!(
            "Unsuccessful transaction, {} -> {}",
            reply.typ.method,
            error_code
        );
    }
    log::trace!("Raw reply to unsuccessful transaction: {:?}", reply);
    Error::UnsuccessfulStunTransaction
}
