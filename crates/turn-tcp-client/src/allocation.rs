//! Client for TURN-over-TCP allocation and binding
//!
//! This client does not own any I/O of its own, nor does it interact with an
//! asynchronous runtime. It provides refresh timestamp tracking,
//! channel binding, and support for authentication including nonce rotation.
//!

use std::{net::SocketAddr, time::Instant};

use crate::{
    auth::{TurnAuth, TurnSession},
    channel::{ChannelInfo, ChannelMap},
    error::Error,
    proto::{self, TurnMessageOwned, TurnTcpRead, TurnTcpWrite},
    timers::Timers,
};
use futures::io::{AsyncRead, AsyncWrite};
use stun::{
    attributes::ATTR_NONCE,
    error_code::{ErrorCodeAttribute, CODE_STALE_NONCE},
    message::{Getter, Message, CLASS_ERROR_RESPONSE, CLASS_SUCCESS_RESPONSE},
    textattrs::{Nonce, TextAttribute},
};
use turn::proto::{
    channum::ChannelNumber, relayaddr::RelayedAddress, reqfamily::RequestedAddressFamily,
};

/// Results from [`TurnAllocation::read`]
///
/// The `Signal` variant should be returned to `handle_signal`.
///
pub enum TurnAllocationRead {
    Signal(TurnReaderSignal),
    ChannelData(ChannelNumber, Vec<u8>),
}

/// Infrequent signals to be sent from the reader task back to the TurnClient
pub enum TurnReaderSignal {
    NonceChanged(Nonce),
}

/// Status information for one channel binding
pub struct ChannelBindStatus {
    pub action: ChannelBindAction,
    pub info: ChannelInfo,
}

/// Which action did we take on the binding in order to access it this time?
pub enum ChannelBindAction {
    Created,
    Refreshed,
    Existing,
}

/// Manage one TURN Allocation using a TURN-over-TCP connection
///
/// TURN allows one allocation per 5-tuple, meaning that for TURN-over-TCP
/// we need a separate socket for each. This type represents a TURN-over-TCP
/// connection that has successfully allocated a relayed UDP port.
///
/// This struct keeps track of bindings and refresh timers.
/// Refreshes can be explicit, but we also send them automatically when
/// outgoing datagrams pass through a channel.
///
/// To establish the allocation, we require a mutable reference to both
/// the read and write side of the connection. Afterward, only the write side
/// is needed to send datagrams. Incoming datagrams can be processed by an
/// independent reader task.
///
/// We require a low-bandwidth feedback channel from the reader back to the
/// writer. This should be implemented by sending `TurnReaderSignal` results
/// from `read` back to `handle_signal`.
///
pub struct TurnAllocation {
    reply: Message,
    timers: Timers,
    session: Option<TurnSession>,
    channels: ChannelMap,
}

impl TurnAllocation {
    /// Establish a new TURN allocation
    ///
    /// This performs authentication if necessary, using the supplied `TurnAuth`.
    ///
    /// Returns only once the allocation has been confirmed. We can't necessarily
    /// send any binding requests, despite the ordered TCP pipe, before the server
    /// has acknowledged the allocation.
    ///
    pub async fn new<W: AsyncWrite + Unpin, R: AsyncRead + Unpin>(
        reader: &mut TurnTcpRead<R>,
        writer: &mut TurnTcpWrite<W>,
        auth: &TurnAuth,
        addr_family: RequestedAddressFamily,
        now: Instant,
    ) -> Result<Self, Error> {
        // Do the authenticated allocation transaction.
        // This requires one or two round trips to the TURN server.
        let (reply, session) = proto::allocate(writer, reader, auth, addr_family).await?;
        if reply.typ.class != CLASS_SUCCESS_RESPONSE {
            return Err(proto::unsuccessful_transaction_error(&reply));
        }
        let timers = Timers::for_allocation_reply(now, &reply)?;
        Ok(Self {
            reply,
            timers,
            session,
            channels: ChannelMap::new(),
        })
    }

    /// Process one incoming message for an allocation.
    ///
    /// This does not need a reference to the TurnAllocation. It should
    /// run on a concurrent task, where datagrams and occasionally
    /// [`TurnReaderSignal`]s may be processed.
    ///
    pub async fn read<R: AsyncRead + Unpin>(
        reader: &mut TurnTcpRead<R>,
    ) -> Result<TurnAllocationRead, Error> {
        loop {
            match reader.read().await? {
                TurnMessageOwned::ChannelData(channel_number, data) => {
                    return Ok(TurnAllocationRead::ChannelData(channel_number, data));
                }
                TurnMessageOwned::Stun(message) => {
                    let mut error_code = ErrorCodeAttribute::default();
                    let mut nonce = TextAttribute {
                        attr: ATTR_NONCE,
                        ..Default::default()
                    };
                    if message.typ.class == CLASS_SUCCESS_RESPONSE {
                        // Successful asynchronous transaction.
                        // We don't have any further action to take here.
                    } else if message.typ.class == CLASS_ERROR_RESPONSE
                        && error_code.get_from(&message).is_ok()
                        && error_code.code == CODE_STALE_NONCE
                        && nonce.get_from(&message).is_ok()
                    {
                        // It's a "stale nonce" error.
                        // Authentication nonces expire, and this is how we get notified of the change
                        return Ok(TurnAllocationRead::Signal(TurnReaderSignal::NonceChanged(
                            nonce,
                        )));
                    } else {
                        // Any other message indicates a problem
                        return Err(proto::unsuccessful_transaction_error(&message));
                    }
                }
            }
        }
    }

    /// Handle a [`TurnReaderSignal`] that was returned by [`Self::read`].
    ///
    /// This is where we get notified to rotate our authorization Nonce,
    /// Signals may be queued and handled asynchronously.
    ///
    pub fn handle_signal(&mut self, signal: TurnReaderSignal, now: Instant) -> Result<(), Error> {
        match signal {
            // Save the new nonce, and remember to refresh the allocation and
            // all still-valid bindings. This is also an efficient time to do
            // a full garbage collection sweep over the binding map.
            //
            TurnReaderSignal::NonceChanged(nonce) => {
                if let Some(session) = &mut self.session {
                    session.nonce = nonce;
                }
                self.timers.set_refresh_now(now);
                self.channels.refresh_all_and_collect_expired(now);
                Ok(())
            }
        }
    }

    /// Get the STUN [`Message`] containing the successful reply to this allocation's request
    pub fn reply(&self) -> &Message {
        &self.reply
    }

    /// Get a reference to the authentication session, if any
    ///
    /// This will include the username, password, and the current nonce value.
    /// Nonce values will be periodically rotated at the server's request.
    /// If the allocation required no authentication, this will return None.
    ///
    pub fn session(&self) -> &Option<TurnSession> {
        &self.session
    }

    /// Return the allocation's relayed address, if it is known
    ///
    /// This will be the "externally" reachable address if the TURN server
    /// is not behind any additional layers of NAT.
    ///
    /// Returns None if the allocation reply did not contain a relayed address.
    ///
    pub fn relayed_address(&self) -> Option<SocketAddr> {
        let mut attr = RelayedAddress::default();
        match attr.get_from(self.reply()) {
            Err(_) => None,
            Ok(()) => Some(SocketAddr::new(attr.ip, attr.port)),
        }
    }

    /// Return the Timers associated with this allocation
    pub fn timers(&self) -> &Timers {
        &self.timers
    }

    /// Refresh the allocation if necessary
    ///
    /// This happens automatically as part of get_refreshed_peer().
    /// If the allocation is already expired, we detect that and return an error.
    /// If the allocation needs to be refreshed, we begin the refresh transaction.
    ///
    pub async fn refresh<W: AsyncWrite + Unpin>(
        &mut self,
        writer: &mut TurnTcpWrite<W>,
        now: Instant,
    ) -> Result<(), Error> {
        if self.timers.is_expired(now) {
            return Err(Error::AllocationExpired);
        }
        if self.timers.needs_refresh(now) {
            proto::refresh(writer, self.session()).await?;
            self.timers = Timers::for_allocation_reply(now, &self.reply)?;
        }
        Ok(())
    }

    /// Get the channel for a peer, creating or refreshing as needed
    ///
    /// Returns a [`ChannelBindStatus`], which includes [`ChannelInfo`] and [`ChannelNumber`].
    /// This information lets the caller invoke additional callbacks or perform additional
    /// tracking based on whether the allocation was created or refreshed, and when it expires.
    ///
    pub async fn get_refreshed_channel<W: AsyncWrite + Unpin>(
        &mut self,
        writer: &mut TurnTcpWrite<W>,
        now: Instant,
        peer: SocketAddr,
    ) -> Result<ChannelBindStatus, Error> {
        // Refresh the allocation first, and catch allocation expired errors.
        self.refresh(writer, now).await?;
        // Re-use the channel number unless we are sure it is expired. TURN
        // will not allow two different channel numbers for the same peer.
        let (info, action) = match self.channels.get_channel_by_peer(peer) {
            None => (None, ChannelBindAction::Created),
            Some(info) => (
                Some(info),
                if info.timers.needs_refresh(now) {
                    ChannelBindAction::Refreshed
                } else {
                    ChannelBindAction::Existing
                },
            ),
        };
        // Allocate a new channel number if we still need one, and make a ChannelInfo we can return.
        let mut info = match info {
            Some(info) => info.to_owned(),
            None => ChannelInfo::new(self.channels.find_unused_channel(now)?, now),
        };
        // New or refreshed bindings both involve issuing a Bind transaction
        if let ChannelBindAction::Created | ChannelBindAction::Refreshed = action {
            proto::bind(writer, self.session(), info.number, peer.to_owned()).await?;
            info.timers = Timers::for_binding(now);
            self.channels
                .insert_or_refresh(peer.to_owned(), info.to_owned());
        }
        Ok(ChannelBindStatus { action, info })
    }

    /// Send a datagram to an arbitrary peer address
    ///
    /// Channel bindings will be created and refreshed as necessary when
    /// they are in use. Unused bindings will expire server-side, and be
    /// garbage collected by the client eventually.
    ///
    /// To report on status of the underlying bindings, we return a
    /// [`ChannelBindStatus`] which can be used to generate events on
    /// binding create, refresh, and expire.
    ///
    /// This client always uses channel bindings rather than data indications.
    /// Binding status can be used as a representation of the active peer set.
    ///
    pub async fn send<W: AsyncWrite + Unpin>(
        &mut self,
        writer: &mut TurnTcpWrite<W>,
        now: Instant,
        peer: SocketAddr,
        data: &[u8],
    ) -> Result<ChannelBindStatus, Error> {
        let status = self.get_refreshed_channel(writer, now, peer).await?;
        writer.write_channel_data(status.info.number, data).await?;
        Ok(status)
    }
}
