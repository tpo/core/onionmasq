package org.torproject.onionmasq;

import static org.torproject.onionmasq.BridgeLineParser.AMP_CACHE;
import static org.torproject.onionmasq.BridgeLineParser.FRONTS;
import static org.torproject.onionmasq.BridgeLineParser.ICE;
import static org.torproject.onionmasq.BridgeLineParser.OBFS4;
import static org.torproject.onionmasq.BridgeLineParser.SNOWFLAKE;
import static org.torproject.onionmasq.BridgeLineParser.SQS_CREDS_STR;
import static org.torproject.onionmasq.BridgeLineParser.SQS_QUEUE_URL;
import static org.torproject.onionmasq.BridgeLineParser.URL;

import android.annotation.TargetApi;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import org.torproject.onionmasq.circuit.Circuit;
import org.torproject.onionmasq.circuit.CircuitCountryCodes;
import org.torproject.onionmasq.errors.CountryCodeException;
import org.torproject.onionmasq.errors.OnionmasqException;
import org.torproject.onionmasq.errors.ProxyStoppedException;
import org.torproject.onionmasq.events.OnionmasqEvent;
import org.torproject.onionmasq.logging.LogObservable;

import java.io.File;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;

import IPtProxy.IPtProxy;

public class OnionMasq {

    private static final String TAG = OnionMasq.class.getSimpleName();
    private static OnionMasq instance;
    private final ConnectivityManager connectivityManager;
    private final Context appContext;
    private ISocketProtect serviceBinder;
    private final OnionmasqEventObservable eventObservable;
    private final CircuitStore circuitStore;

    private volatile boolean isRunning;

    private BridgeLineParser.IPtConfig selectedIPtConfig;
    private final static Object BRIDGE_CONFIG_LOCK = new Object();

    private final ServiceConnection connection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            if (service instanceof ISocketProtect) {
                serviceBinder =  (ISocketProtect) service;
            } else {
                throw new IllegalArgumentException("Bound service needs to implement ISocketProtect interface");
            }

        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            serviceBinder = null;
        }
    };

    private static OnionMasq getInstance() throws IllegalStateException {
        if (instance == null) {
            throw new IllegalStateException("OnionmasqHelper is not initialized");
        }
        return instance;
    }

    public static void init(Context context) throws OnionmasqException {
        if (instance == null) {
            instance = new OnionMasq(context);
        }
    }

    private OnionMasq(Context context) throws OnionmasqException {
        this.appContext = context.getApplicationContext();
        this.connectivityManager = (ConnectivityManager) appContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        this.eventObservable = new OnionmasqEventObservable();
        this.circuitStore = new CircuitStore();
        OnionMasqJni.init();
    }

    @TargetApi(Build.VERSION_CODES.Q)
    static int getConnectionOwnerUid(int protocol, byte[] rawSourceAddress, int sourcePort, byte[] rawDestinationAddress, int destinationPort) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            try {
                InetSocketAddress sourceSocketAddress = new InetSocketAddress(InetAddress.getByAddress(rawSourceAddress), sourcePort);
                InetSocketAddress destinationSocketAddress = new InetSocketAddress(InetAddress.getByAddress(rawDestinationAddress), destinationPort);
                return getInstance().connectivityManager.getConnectionOwnerUid(protocol, sourceSocketAddress, destinationSocketAddress);
            } catch (UnknownHostException | IllegalStateException e) {
                e.printStackTrace();
            }
        }

        return -1; // Process.INVALID_UID
    }

    public static void bindVPNService(Class vpnServiceClass) {
        Intent intent = new Intent(getInstance().appContext, vpnServiceClass);
        getInstance().appContext.bindService(intent, getInstance().connection, Context.BIND_AUTO_CREATE);
    }

    public static void unbindVPNService() {
        try {
            getInstance().appContext.unbindService(getInstance().connection);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    public static synchronized void start(int fileDescriptor) throws OnionmasqException {
        start(fileDescriptor, null);
    }

    public static synchronized void start(int fileDescriptor, String bridgeLines) throws OnionmasqException {
        getInstance().isRunning = true;
        try {
            BridgeLineParser.IPtConfig config = BridgeLineParser.getIPtConfigFrom(bridgeLines);
            startIPtProxy(config);
            OnionMasqJni.runProxy(
                    fileDescriptor,
                    // TODO: revert to cache dir, after arti has fixed handling of corrupted tor cache directory
                    getInstance().appContext.getFilesDir().getAbsolutePath(),
                    getInstance().appContext.getFilesDir().getAbsolutePath(),
                    config == null ? null : config.getBridgeType(),
                    config == null ? 0 : config.getPtClientPort(),
                    config == null ? null : config.getBridgeLines()
            );
        } finally {
            getInstance().isRunning = false;
        }
    }

    private static void startIPtProxy(BridgeLineParser.IPtConfig iPtConfig) {
        synchronized (BRIDGE_CONFIG_LOCK) {
            if (iPtConfig == null || iPtConfig.getBridgeConfigs().isEmpty()) {
                return;
            }
            File ptDir = new File(getInstance().appContext.getCacheDir().getAbsolutePath(), "pt_state");
            IPtProxy.setStateLocation(ptDir.getAbsolutePath());
            long port = -1;
            switch (iPtConfig.getBridgeType()) {
                case OBFS4 -> port = IPtProxy.startLyrebird("DEBUG", true, false, null);
                case SNOWFLAKE -> {
                    port = IPtProxy.startSnowflake(iPtConfig.getOption(ICE),
                            iPtConfig.getOption(URL),
                            iPtConfig.getOption(FRONTS),
                            iPtConfig.getOption(AMP_CACHE),
                            iPtConfig.getOption(SQS_QUEUE_URL),
                            iPtConfig.getOption(SQS_CREDS_STR),
                            "snowflake.log",
                            true,
                            true,
                            false,
                            5);
                }
                default -> Log.e(TAG, "unknown bridge type");
            }
            iPtConfig.setPtClientPort(port);

            getInstance().selectedIPtConfig = iPtConfig;
        }
    }

    private static void stopIPtProxy() {
        synchronized (BRIDGE_CONFIG_LOCK) {
            BridgeLineParser.IPtConfig config = getInstance().selectedIPtConfig;
            if (config == null) {
                return;
            }
            if (SNOWFLAKE.equals(config.getBridgeType())) {
                IPtProxy.stopSnowflake();
            } else if (OBFS4.equals(config.getBridgeType())){
                IPtProxy.stopLyrebird();
            } else {
                throw new IllegalStateException("Trying to stop invalid pluggable transport type.");
            }
            getInstance().selectedIPtConfig = null;
        }
    }

    public static boolean isRunning() {
        return getInstance().isRunning;
    }

    public static void stop() {
        OnionMasqJni.closeProxy();
        OnionMasqJni.resetCounters();
        getInstance().circuitStore.reset();
        stopIPtProxy();
    }

    public static void refreshCircuits() throws ProxyStoppedException {
        OnionMasqJni.refreshCircuits();
        getInstance().circuitStore.reset();
    }

    public static void refreshCircuitsForApp(long appId) throws ProxyStoppedException {
        OnionMasqJni.refreshCircuitsForApp(appId);
        getInstance().circuitStore.removeCircuitCountryCodes((int)appId);
    }

    static boolean protect(int socket)  {
        if (getInstance().serviceBinder == null) {
            LogObservable.getInstance().addLog("Cannot protect Socket " + socket + ". VpnService is not registered.");
            return false;
        }

        return  getInstance().serviceBinder.protect(socket);
    }

    public static MutableLiveData<OnionmasqEvent> getEventObservable() {
        return getInstance().eventObservable.getEvent();
    }

    static void handleEvent(OnionmasqEvent event) {
        getInstance().circuitStore.handleEvent(event);
        getInstance().eventObservable.update(event);
    }

    public static long getBytesReceived() {
        return OnionMasqJni.getBytesReceived();
    }

    public static long getBytesReceivedForApp(long appId) { return OnionMasqJni.getBytesReceivedForApp(appId); };

    public static long getBytesSent() {
        return OnionMasqJni.getBytesSent();
    }

    public static long getBytesSentForApp(long appId) { return OnionMasqJni.getBytesSentForApp(appId); };

    public static void resetCounters() {
        OnionMasqJni.resetCounters();
    }

    public static void setCountryCode(String cc) throws CountryCodeException { OnionMasqJni.setCountryCode(cc); }

    public static void setTurnServerConfig(String host, long port, String auth) {
        OnionMasqJni.setTurnServerConfig(host, port, auth);
    }

    public static void setBridgeLines(String bridgeLines) {
        OnionMasqJni.setBridgeLines(bridgeLines);
    }

    public static ArrayList<Circuit> getCircuitsForAppUid(int appId) {
        return getInstance().circuitStore.getCircuitsForAppUid(appId);
    }

    public static ArrayList<CircuitCountryCodes> getCircuitCountryCodesForAppUid(int appId) {
        return getInstance().circuitStore.getCircuitCountryCodesForAppUid(appId);
    }
}
