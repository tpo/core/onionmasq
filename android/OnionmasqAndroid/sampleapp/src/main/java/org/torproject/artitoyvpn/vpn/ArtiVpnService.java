package org.torproject.artitoyvpn.vpn;

import static org.torproject.artitoyvpn.vpn.VpnStatusObservable.Status.ERROR;
import static org.torproject.artitoyvpn.vpn.VpnStatusObservable.Status.RUNNING;
import static org.torproject.artitoyvpn.vpn.VpnStatusObservable.Status.STARTING;
import static org.torproject.artitoyvpn.vpn.VpnStatusObservable.Status.STOPPED;

import android.app.Notification;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.VpnService;
import android.os.Binder;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.ParcelFileDescriptor;
import android.system.OsConstants;
import android.util.Log;

import androidx.annotation.WorkerThread;
import androidx.lifecycle.Observer;

import org.torproject.artitoyvpn.R;
import org.torproject.artitoyvpn.utils.PreferenceHelper;
import org.torproject.artitoyvpn.utils.VpnNotificationManager;
import org.torproject.onionmasq.ConnectivityHandler;
import org.torproject.onionmasq.ISocketProtect;
import org.torproject.onionmasq.OnionMasq;
import org.torproject.onionmasq.errors.CountryCodeException;
import org.torproject.onionmasq.errors.OnionmasqException;
import org.torproject.onionmasq.events.BootstrapEvent;
import org.torproject.onionmasq.events.ClosedConnectionEvent;
import org.torproject.onionmasq.events.ConnectivityEvent;
import org.torproject.onionmasq.events.FailedConnectionEvent;
import org.torproject.onionmasq.events.NewConnectionEvent;
import org.torproject.onionmasq.events.NewDirectoryEvent;
import org.torproject.onionmasq.events.OnionmasqEvent;
import org.torproject.onionmasq.events.RelayDetails;
import org.torproject.onionmasq.events.VPNetworkLostEvent;
import org.torproject.onionmasq.logging.LogHelper;
import org.torproject.onionmasq.logging.LogObservable;
import org.torproject.onionmasq.utils.OnionmasqThreadpoolExecutor;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

public class ArtiVpnService extends VpnService implements ISocketProtect {
    static final String TAG = ArtiVpnService.class.getSimpleName();
    public static final String ACTION_START_VPN = TAG + ".start";
    public static final String ACTION_STOP_VPN = TAG + ".stop";
    private VpnNotificationManager notificationManager;
    private final static int ALWAYS_ON_MIN_API_LEVEL = Build.VERSION_CODES.N;
    private LogHelper logHelper;
    private Observer<OnionmasqEvent> observer;
    private ConnectivityHandler connectivityHandler;

    private Handler mainHandler;
    private OnionmasqThreadpoolExecutor es;

    private PreferenceHelper preferenceHelper;

    private Timer timer;

    /** Maximum packet size is constrained by the MTU, which is given as a signed short. */
    private static final int MAX_PACKET_SIZE = Short.MAX_VALUE;

    private final IBinder binder = new ArtiVpnServiceBinder(this);

    public static class ArtiVpnServiceBinder extends Binder implements ISocketProtect {
        WeakReference<ISocketProtect> socketProtectWeakReference;

        public ArtiVpnServiceBinder(ISocketProtect socketProtect) {
            this.socketProtectWeakReference = new WeakReference<>(socketProtect);
        }
        @Override
        public boolean protect(int socket) {
            try {
                return socketProtectWeakReference.get().protect(socket);
            } catch (NullPointerException npe) {
                npe.printStackTrace();
            }
            return false;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        notificationManager = new VpnNotificationManager(this);
        logHelper = new LogHelper();
        OnionMasq.bindVPNService(ArtiVpnService.class);
        mainHandler = new Handler(getMainLooper());
        timer = new Timer();
        preferenceHelper = new PreferenceHelper(this);
        OnionMasq.setTurnServerConfig(preferenceHelper.getTurnServerHost(), preferenceHelper.getTurnServerPort(), preferenceHelper.getTurnServerAuth());
        try {
            OnionMasq.setCountryCode(preferenceHelper.getExitNodeCountry());
        } catch (CountryCodeException e) {
            throw new RuntimeException(e);
        }
        connectivityHandler = new ConnectivityHandler(this);
        connectivityHandler.register();

        es = new OnionmasqThreadpoolExecutor();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Notification notification = notificationManager.buildForegroundServiceNotification();
        startForeground(VpnNotificationManager.ARTI_NOTIFICATION_ID, notification);
        String action = intent != null ? intent.getAction() : "";
        if (ACTION_START_VPN.equals(action) ||
                "android.net.VpnService".equals(action) && Build.VERSION.SDK_INT >= ALWAYS_ON_MIN_API_LEVEL) {
                //only always-on feature triggers this
            Log.d(TAG, "submit ACTION_START_VPN");
            es.submit(() -> {
                Log.d(TAG, "call ACTION_START_VPN");
                ArtiVpnService.this.establishVpn();
            });

        } else if (ACTION_STOP_VPN.equals(action)) {
            Log.d(TAG, "submit ACTION_STOP_VPN");
            es.submit(() -> {
                Log.d(TAG, "call ACTION_STOP_VPN");
                ArtiVpnService.this.stop();
            });
        }
        return START_STICKY;
    }

    @Override
    public void onRevoke() {
        Log.d(TAG, "submit ACTION_STOP_VPN onRevoke");
        es.submit(() -> {
            Log.d(TAG, "call ACTION_STOP_VPN onRevoke");
            ArtiVpnService.this.stop();
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroyCalled");
        notificationManager.cancelNotifications();
        connectivityHandler.unregister();
        connectivityHandler = null;
        es = null;
        logHelper = null;
        if (VpnStatusObservable.getStatus().getValue() != ERROR) {
            VpnStatusObservable.update(STOPPED);
        }
    }

    @WorkerThread
    private void stop() {
        VpnStatusObservable.update(VpnStatusObservable.Status.STOPPING);
        es.shutdown();
        OnionMasq.stop();
        logHelper.stopLog();
        timer.cancel();
        VpnStatusObservable.resetData();
        mainHandler.post(() -> {
            OnionMasq.getEventObservable().removeObserver(observer);
            es.awaitTermination();
            stopForeground(true);
            OnionMasq.unbindVPNService();
            stopSelf();
        });
    }

    private Builder prepareVpnProfile() {
        Builder builder = new Builder();
        applyAppFilter(builder);
        builder.setSession("Arti dummy session");
        builder.addRoute("0.0.0.0", 0);
        builder.addRoute("::",0);
        builder.addAddress("169.254.42.1", 16);
        builder.addAddress("fc00::", 7);
        builder.addDnsServer("169.254.42.53");
        builder.addDnsServer("fe80::53");

        builder.allowFamily(OsConstants.AF_INET);
        builder.allowFamily(OsConstants.AF_INET6);
        builder.setMtu(1500);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            builder.setMetered(false);
        }
        return builder;
    }

    @WorkerThread
    private void establishVpn() {
        VpnStatusObservable.update(STARTING);
        try {
            Builder builder = prepareVpnProfile();
            if (OnionMasq.isRunning()) {
                LogObservable.getInstance().addLog("onionmasq is running...  stopping");
                OnionMasq.stop();
            } else {
                LogObservable.getInstance().addLog("onionmasq not running...  setting up observers");
                logHelper.readLog();
                observer = new OnionmasqObserver(this);
                mainHandler.post(() -> {
                    OnionMasq.getEventObservable().observeForever(observer);
                });
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        VpnStatusObservable.updateDataUsage(OnionMasq.getBytesReceived(), OnionMasq.getBytesSent());
                    }
                }, 1, 1000);
            }
            ParcelFileDescriptor fd = builder.establish();
            Thread onionThread = new Thread(() -> {
                try {
                    OnionMasq.start(fd.detachFd(), preferenceHelper.getBridgeLines());
                } catch (OnionmasqException e) {
                    // FIXME(eta): We probably want to actually show the error to the user here!
                    e.printStackTrace();
                    VpnStatusObservable.update(ERROR);
                    stopSelf();
                }
            });
            onionThread.start();
        } catch (Exception e) {
            // Catch any exception
            OnionMasq.stop();
            e.printStackTrace();
            timer.cancel();
            VpnStatusObservable.resetData();
            VpnStatusObservable.update(ERROR);
            stopSelf();
        }
    }

    /**
     * Adds selected app into 'allowed apps' for current vpn connection. Only selected apps will use VPN.
     * @param builder VPN Builder
     */
    private void applyAppFilter(Builder builder){
        Set<String> selectedApps = preferenceHelper.getSelectedApps();
        PackageManager packageManager = getPackageManager();
        if (selectedApps == null) {
            List<ApplicationInfo> allApps = packageManager.getInstalledApplications(PackageManager.GET_META_DATA);
            //No selection done, so we allow no apps.
            for (ApplicationInfo appPackageInfo: allApps) {
                try {
                    builder.addDisallowedApplication(appPackageInfo.packageName);
                } catch (PackageManager.NameNotFoundException e) {
                    // The app is selected but isn't installed anymore.
                }
            }
            try {
                builder.addDisallowedApplication(getPackageName());
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
            return;
        }

        for (String appPackage: selectedApps){
            try {
                packageManager.getPackageInfo(appPackage, 0);
                if (!appPackage.equals(getPackageName())) {
                    builder.addAllowedApplication(appPackage);
                }
            } catch (PackageManager.NameNotFoundException e) {
                // The app is selected but isn't installed anymore.
            }
        }
    }

    private static class OnionmasqObserver implements Observer<OnionmasqEvent> {

        WeakReference<Context> contextRef;
        public OnionmasqObserver(Context context) {
            contextRef = new WeakReference<>(context);
        }

        @Override
        public void onChanged(OnionmasqEvent onionmasqEvent) {
            try {
                if (onionmasqEvent instanceof BootstrapEvent event) {
                    if (event.isReadyForTraffic) {
                        VpnStatusObservable.update(RUNNING);
                    }
                    LogObservable.getInstance().addLog(contextRef.get().getString(R.string.bootstrap_at, event.bootstrapStatus));
                }
                else if (onionmasqEvent instanceof NewConnectionEvent event) {
                    LogObservable.getInstance().addLog(contextRef.get().getString(R.string.new_connection, event.proxySrc, event.proxyDst, event.torDst, event.appId));
                    int i = 0;
                    for (RelayDetails relay : event.circuit) {
                        String identity = "unknown";
                        String address = "unknown";
                        String country = "??";
                        if (relay.ed_identity != null) {
                            identity = relay.ed_identity;
                        }
                        if (relay.rsa_identity != null) {
                            identity = relay.rsa_identity;
                        }
                        if (relay.country_code != null) {
                            country = relay.country_code;
                        }
                        if (relay.addresses.size() > 0) {
                            address = relay.addresses.get(0);
                        }
                        LogObservable.getInstance().addLog(contextRef.get().getString(R.string.new_connection_hop, event.proxySrc, event.proxyDst, i, address, country, identity));
                        i++;
                    }
                }
                else if (onionmasqEvent instanceof FailedConnectionEvent event) {
                    LogObservable.getInstance().addLog(contextRef.get().getString(R.string.failed_connection, event.proxySrc, event.proxyDst, event.torDst, event.error, event.appId));
                }
                else if (onionmasqEvent instanceof ClosedConnectionEvent event) {
                    if (event.error != null) {
                        LogObservable.getInstance().addLog(contextRef.get().getString(R.string.failed_connection_closed, event.proxySrc, event.proxyDst, event.error));
                    }
                    else {
                        LogObservable.getInstance().addLog(contextRef.get().getString(R.string.closed_connection, event.proxySrc, event.proxyDst));
                    }
                }
                else if (onionmasqEvent instanceof NewDirectoryEvent event) {
                    LogObservable.getInstance().addLog("New directory received.");

                    List<Map.Entry<String, Long>> relaysByCountry = new ArrayList(event.relaysByCountry.entrySet());
                    relaysByCountry.sort((x, y) -> x.getValue().compareTo(y.getValue()));
                    for (Map.Entry<String, Long> entry : relaysByCountry) {
                        LogObservable.getInstance().addLog(contextRef.get().getString(R.string.relay_country_count, entry.getKey(), entry.getValue()));
                    }
                }
                else if (onionmasqEvent instanceof ConnectivityEvent event) {
                    Log.d(TAG, "onionmasq event: Connectivity event: " + event.hasInternetConnectivity);
                    VpnStatusObservable.updateConnectivity(event.hasInternetConnectivity);
                    LogObservable.getInstance().addLog(contextRef.get().getString(R.string.connectivity_changed, event.hasInternetConnectivity));
                } else if (onionmasqEvent instanceof VPNetworkLostEvent) {
                    // Always-on was turned off, while the VPN was running. Try to restart VPN or stop gracefully.
                    if (OnionMasq.isRunning()) {
                        tryToRecoverVPN();
                    }
                }
            } catch (NullPointerException npe) {
                npe.printStackTrace();
            }
        }

        private void tryToRecoverVPN() {
            Context c = contextRef.get();
            if (c != null) {
                Intent intent = VpnService.prepare(c.getApplicationContext());
                if (intent == null) {
                    VpnServiceCommand.startVpn(c);
                } else {
                    VpnServiceCommand.stopVpn(c);
                }
            }
        }
    }
}
