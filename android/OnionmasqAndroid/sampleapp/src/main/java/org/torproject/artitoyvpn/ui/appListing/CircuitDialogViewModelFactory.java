package org.torproject.artitoyvpn.ui.appListing;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider.Factory;

public class CircuitDialogViewModelFactory implements Factory {
    private Application application;
    private int appUID;

    public CircuitDialogViewModelFactory(Application application, int appUID) {
        this.application = application;
        this.appUID = appUID;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new CircuitDialogViewModel(application, appUID);
    }
}
