package org.torproject.artitoyvpn.vpn;

import static org.torproject.artitoyvpn.vpn.VpnStatusObservable.Status.STOPPED;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import org.torproject.artitoyvpn.R;

import org.torproject.artitoyvpn.utils.Utils;
import org.torproject.onionmasq.events.OnionmasqEvent;
import org.torproject.onionmasq.logging.LogObservable;

public class VpnStatusObservable {
    public enum Status {
        STOPPED,
        STARTING_UI,
        STARTING,
        RUNNING,
        STOPPING_UI,
        STOPPING,
        ERROR;

        public int getStateDescriptionResId() {
            return switch (this) {
                case STARTING -> R.string.state_description_starting;
                case RUNNING -> R.string.state_description_running;
                case STOPPING -> R.string.state_description_stopping;
                case STOPPED -> R.string.state_description_off;
                case ERROR -> R.string.state_description_error;
                default -> 0;
            };
        }
    }

    private final MutableLiveData<DataUsage> dataUsage;

    private final MutableLiveData<Status> statusLiveData;

    private final MutableLiveData<Boolean> hasInternetConnectivity;

    private static VpnStatusObservable instance;

    private VpnStatusObservable() {
        statusLiveData = new MutableLiveData<>(STOPPED);
        dataUsage = new MutableLiveData<>(new DataUsage());
        hasInternetConnectivity = new MutableLiveData<>(false);
    }

    public static VpnStatusObservable getInstance() {
        if (instance == null) {
            instance = new VpnStatusObservable();
        }
        return instance;
    }

    public static void update(Status status) {
        Log.d("STATUS", "update to " + status.toString());
        if (Utils.isRunningOnMainThread()) {
            instance.statusLiveData.setValue(status);
        } else {
            instance.statusLiveData.postValue(status);
        }
        LogObservable.getInstance().addLog(status.toString());
    }

    public static void updateConnectivity(boolean hasInternetConnectivity) {
        instance.hasInternetConnectivity.postValue(hasInternetConnectivity);
    }

    public static void updateDataUsage(long downstream, long upstream) {
        DataUsage lastDataUsage = instance.dataUsage.getValue();
        DataUsage updatedDataUsage = new DataUsage();
        updatedDataUsage.downstreamData = downstream;
        updatedDataUsage.upstreamData = upstream;
        long timeDelta = Math.max((updatedDataUsage.timeStamp - lastDataUsage.timeStamp) / 1000, 1);
        updatedDataUsage.upstreamDataPerSec = (updatedDataUsage.upstreamData - lastDataUsage.upstreamData) / timeDelta;
        updatedDataUsage.downstreamDataPerSec = (updatedDataUsage.downstreamData - lastDataUsage.downstreamData) / timeDelta;
        if (Utils.isRunningOnMainThread()) {
            instance.dataUsage.setValue(updatedDataUsage);
        } else {
            instance.dataUsage.postValue(updatedDataUsage);
        }
    }

    public static void resetData() {
        if (Utils.isRunningOnMainThread()) {
            instance.dataUsage.setValue(new DataUsage());
        } else {
            instance.dataUsage.postValue(new DataUsage());
        }
    }

    public static LiveData<DataUsage> getDataUsage() {
        return instance.dataUsage;
    }

    public static LiveData<Status> getStatus() {
        return instance.statusLiveData;
    }

    public static LiveData<Boolean> hasInternetConnectivity() {
        return instance.hasInternetConnectivity;
    }

    public static class VpnConnectionStatus {
        public final Status status;
        public final OnionmasqEvent event;
        public final boolean hasInternet;

        public VpnConnectionStatus(Status status, OnionmasqEvent event, boolean hasInternet) {
            this.status = status;
            this.event = event;
            this.hasInternet = hasInternet;
        }
    }

}
