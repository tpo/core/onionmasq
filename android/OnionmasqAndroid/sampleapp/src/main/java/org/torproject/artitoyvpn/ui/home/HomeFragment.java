package org.torproject.artitoyvpn.ui.home;

import static org.torproject.artitoyvpn.vpn.VpnStatusObservable.Status.STOPPED;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.VpnService;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.torproject.artitoyvpn.R;
import org.torproject.artitoyvpn.databinding.FragmentHomeBinding;
import org.torproject.artitoyvpn.vpn.VpnServiceCommand;
import org.torproject.artitoyvpn.vpn.VpnStatusObservable;
import org.torproject.artitoyvpn.vpn.VpnStatusObservable.Status;
import org.torproject.onionmasq.OnionMasq;
import org.torproject.onionmasq.errors.ProxyStoppedException;

public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;
    private FragmentHomeBinding binding;

    ActivityResultLauncher<Intent> startForResult = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(),
            result -> {
                if (result.getResultCode() == Activity.RESULT_OK) {
                    VpnServiceCommand.startVpn(getContext());
                } else {
                    VpnStatusObservable.update(Status.ERROR);
                }
            });

    ActivityResultLauncher<String> notificationRequestLauncher = registerForActivityResult(new ActivityResultContracts.RequestPermission(), granted -> {
        if (!granted) {
            Toast.makeText(this.getContext(), "You won't see up- and download stats in the notification bar.", Toast.LENGTH_LONG).show();
        }
        handleVpnButton(this.getContext());
    });

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                new ViewModelProvider(this).get(HomeViewModel.class);

        binding = FragmentHomeBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        final TextView textView = binding.textVpnState;
        homeViewModel.getText().observe(getViewLifecycleOwner(), textView::setText);

        // Create a LiveData to combine VPN status, internet connectivity and OnionMasq events
        LiveData<VpnStatusObservable.VpnConnectionStatus> vpnConnectionStatus = Transformations.switchMap(
                VpnStatusObservable.getStatus(),
                status -> Transformations.switchMap(
                        VpnStatusObservable.hasInternetConnectivity(),
                        hasInternet -> Transformations.map(
                                OnionMasq.getEventObservable(),
                                event -> new VpnStatusObservable.VpnConnectionStatus(status, event, hasInternet)
                        )
                )
        );
        vpnConnectionStatus.observe(getViewLifecycleOwner(), vpnStatus -> {
            homeViewModel.update(vpnStatus.event, getContext(), vpnStatus.status, vpnStatus.hasInternet);
        });
        final TextView progressTextView = binding.textProgress;
        homeViewModel.getProgressText().observe(getViewLifecycleOwner(), progressTextView::setText);

        VpnStatusObservable.getDataUsage().observe(getViewLifecycleOwner(), dataUsage -> {
            boolean hasInet = Boolean.TRUE.equals(VpnStatusObservable.hasInternetConnectivity().getValue());
            homeViewModel.updateDataUsage(root.getContext(), dataUsage, hasInet);
        });
        homeViewModel.getOverallThroughputText().observe(getViewLifecycleOwner(), s -> binding.textBandwidth.setText(s));
        homeViewModel.getThroughputText().observe(getViewLifecycleOwner(), s -> binding.textBandwidthPerSec.setText(s));

        final FloatingActionButton refreshCircuitsButton = binding.buttonRefreshCircuits;
        refreshCircuitsButton.setOnClickListener(view -> {
            try {
                OnionMasq.refreshCircuits();
                Toast.makeText(view.getContext(), "Circuits refreshed.", Toast.LENGTH_SHORT).show();
            }
            catch (ProxyStoppedException e) {
                Toast.makeText(view.getContext(), "Error: VPN is not running!", Toast.LENGTH_SHORT).show();
            }
        });

        final Button vpnButton = binding.buttonMain;
        VpnStatusObservable.getStatus().observe(getViewLifecycleOwner(), status -> {
            boolean hasInet = Boolean.TRUE.equals(VpnStatusObservable.hasInternetConnectivity().getValue());
            homeViewModel.update(status, getContext(),hasInet);
        });
        homeViewModel.getButtonText().observe(getViewLifecycleOwner(), vpnButton::setText);
        vpnButton.setOnClickListener(view -> {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU &&
                    homeViewModel.askForNotificationPermission()) {
                    notificationRequestLauncher.launch(Manifest.permission.POST_NOTIFICATIONS);
                return;
            }
            handleVpnButton(view.getContext());
        });

        binding.exitNodeCountry.setText(homeViewModel.getExitNodeCountry());
        return root;
    }

    private void handleVpnButton(Context context) {
        Status status = VpnStatusObservable.getStatus().getValue();
        Log.d("HANDLE_VPN_BTN", "handle " + status);
        switch (status != null ? status : STOPPED) {
            case STOPPING:
            case STARTING:
            case RUNNING:
                VpnStatusObservable.update(Status.STOPPING_UI);
                VpnServiceCommand.stopVpn(context);
                break;
            case STOPPED:
            case ERROR:
                VpnStatusObservable.update(Status.STARTING_UI);
                prepareToStartVPN();
                break;
            default:
                Log.d("HANDLE_VPN_BTN", "Ignored UI status: " + status);
                break;

        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        homeViewModel.stateText.setValue(getContext().getString(R.string.state_description_off));
        homeViewModel.buttonText.setValue(getContext().getString(R.string.start));
    }

    public void prepareToStartVPN() {
        Intent vpnIntent = null;
        try {
            vpnIntent = VpnService.prepare(getContext().getApplicationContext()); // stops the VPN connection created by another application.
        } catch (NullPointerException npe) {
            VpnStatusObservable.update(Status.ERROR);
        }
        if (vpnIntent != null) {
            startForResult.launch(vpnIntent);
        } else {
            VpnServiceCommand.startVpn(getContext());
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}