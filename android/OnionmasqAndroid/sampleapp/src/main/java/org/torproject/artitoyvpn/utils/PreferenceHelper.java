package org.torproject.artitoyvpn.utils;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.Set;

public class PreferenceHelper {
    private final String preferenceName = "ArtyToyVPNPRref";
    public static final String selectedAppSetName = "selectedAppSet";
    private final String showLogTimestamps = "showLogTimestamps";
    private final String exitNodeCountry = "exitNodeCountry";
    private final String bridgeLines = "bridgeLines";
    private final String turnServerHost = "turnServerHost";
    private final String turnServerPort = "turnServerPort";
    private final String turnServerAuth = "turnServerAuth";

    SharedPreferences sharedPreferences;

    public PreferenceHelper(Context context) {
        sharedPreferences = context.getApplicationContext().getSharedPreferences(preferenceName, Context.MODE_PRIVATE);
    }

    public void registerPreferenceChangeListener(SharedPreferences.OnSharedPreferenceChangeListener listener ) {
        sharedPreferences.registerOnSharedPreferenceChangeListener(listener);
    }

    public void removePreferenceChangeListener(SharedPreferences.OnSharedPreferenceChangeListener listener) {
        sharedPreferences.unregisterOnSharedPreferenceChangeListener(listener);
    }

    /**
     *
     * @return A Set of strings if selection is done(all selected, some selected or none selected), null otherwise().
     */
    public Set<String> getSelectedApps() {
        return sharedPreferences.getStringSet(selectedAppSetName, null);
    }

    //apply is important, otherwise this may freeze the main thread for few millis.
    public void setSelectedApps(Set<String> selectedApps) {
        sharedPreferences.edit().putStringSet(selectedAppSetName, selectedApps.isEmpty() ? null : selectedApps).apply();
    }

    public void showLogTimestamps(Boolean show) {
        sharedPreferences.edit().putBoolean(showLogTimestamps, show).apply();
    }

    public Boolean getShowLogTimestamps() {
        return sharedPreferences.getBoolean(showLogTimestamps, false);
    }

    public void setExitNodeCountry(String countryCode) {
        sharedPreferences.edit().putString(exitNodeCountry, countryCode).apply();
    }

    public String getExitNodeCountry() {
        return sharedPreferences.getString(exitNodeCountry, null);
    }

    public void setBridgeLines(String bridges) {
        sharedPreferences.edit().putString(bridgeLines, bridges).apply();
    }

    public String getBridgeLines() {
        return sharedPreferences.getString(bridgeLines, null);
    }

    public String getTurnServerHost() {
        return sharedPreferences.getString(turnServerHost, null);
    }

    public void setTurnServerHost(String host) {
        sharedPreferences.edit().putString(turnServerHost, host).apply();
    }

    public long getTurnServerPort() {
        return sharedPreferences.getLong(turnServerPort, 0);
    }

    public void setTurnServerPort(long host) {
        sharedPreferences.edit().putLong(turnServerPort, host).apply();
    }

    public String getTurnServerAuth() {
        return sharedPreferences.getString(turnServerAuth, null);
    }

    public void setTurnServerAuth(String host) {
        sharedPreferences.edit().putString(turnServerAuth, host).apply();
    }
}
