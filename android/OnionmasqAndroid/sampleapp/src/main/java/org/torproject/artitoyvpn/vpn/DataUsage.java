package org.torproject.artitoyvpn.vpn;

public class DataUsage {
    public long upstreamData;
    public long downstreamData;
    public long upstreamDataPerSec;
    public long downstreamDataPerSec;
    public long timeStamp;

    public DataUsage() {
        timeStamp = System.currentTimeMillis();
    }


}
