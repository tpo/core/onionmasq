package org.torproject.artitoyvpn.ui.home;

import static org.torproject.artitoyvpn.vpn.VpnStatusObservable.Status.STARTING;

import android.Manifest;
import android.app.Application;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.text.format.Formatter;

import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import org.torproject.artitoyvpn.R;
import org.torproject.artitoyvpn.utils.PreferenceHelper;
import org.torproject.artitoyvpn.vpn.DataUsage;
import org.torproject.artitoyvpn.vpn.VpnStatusObservable;
import org.torproject.onionmasq.events.BootstrapEvent;
import org.torproject.onionmasq.events.ConnectivityEvent;
import org.torproject.onionmasq.events.OnionmasqEvent;

import java.util.Locale;

public class HomeViewModel extends AndroidViewModel {

    final MutableLiveData<String> stateText;
    final MutableLiveData<String> buttonText;
    final MutableLiveData<String> progressText;

    private final MutableLiveData<String> throughputText;
    private final MutableLiveData<String> throughputTextOverall;

    private final PreferenceHelper preferenceHelper;

    public HomeViewModel(Application application) {
        super(application);
        stateText = new MutableLiveData<>();
        buttonText = new MutableLiveData<>();
        progressText = new MutableLiveData<>();
        throughputText = new MutableLiveData<>();
        throughputTextOverall = new MutableLiveData<>();
        preferenceHelper = new PreferenceHelper(application);
    }

    public LiveData<String> getText() {
        return stateText;
    }

    public LiveData<String> getButtonText() {
        return buttonText;
    }

    public LiveData<String> getProgressText() {
        return progressText;
    }

    public LiveData<String> getThroughputText() {
        return throughputText;
    }

    public LiveData<String> getOverallThroughputText() {
        return throughputTextOverall;
    }

    public void updateDataUsage(Context context, DataUsage dataUsage, boolean hasInet) {
        String received = Formatter.formatFileSize(context, hasInet ? dataUsage.downstreamDataPerSec : 0);
        String sent = Formatter.formatFileSize(context, hasInet ? dataUsage.upstreamDataPerSec : 0);
        throughputText.postValue(context.getString(R.string.bandwidth_stats_per_sec, received, sent));

        if (hasInet) {
            String receivedOverall = Formatter.formatFileSize(context, dataUsage.downstreamData);
            String sentOverall = Formatter.formatFileSize(context, dataUsage.upstreamData);
            throughputTextOverall.postValue(context.getString(R.string.bandwidth_stats, receivedOverall, sentOverall));
        }
    }

    public void update(OnionmasqEvent onionmasqEvent, Context context, VpnStatusObservable.Status status, boolean hasInternetConnectivity) {
        if (onionmasqEvent instanceof BootstrapEvent event) {
            if (status != STARTING) {
                return;
            }

            if (!hasInternetConnectivity) {
                update(status, context, false);
                return;
            }

            if (event.bootstrapStatus != null) {
                progressText.postValue(event.bootstrapStatus);
            } else {
                progressText.postValue(context.getString(R.string.bootstrap_at, (event.bootstrapPercent + "%")));
            }
        } else if (onionmasqEvent instanceof ConnectivityEvent) {
            update(status, context, hasInternetConnectivity);
        }
    }

    public void update(VpnStatusObservable.Status status, Context context, boolean hasInternetConnectivity) {
        switch (status) {
            case STARTING:
            case RUNNING:
            case STOPPING:
                stateText.postValue(context.getString(status.getStateDescriptionResId())); // See point 2
                buttonText.postValue(context.getString(R.string.stop));
                updateConnectivityStatus(context, hasInternetConnectivity);
                break;
            case STOPPED:
            case ERROR:
                stateText.postValue(context.getString(status.getStateDescriptionResId())); // See point 2
                buttonText.postValue(context.getString(R.string.start));
                progressText.postValue(null);
                break;
        }
    }

    private void updateConnectivityStatus(Context context, boolean hasInternetConnectivity) {
        if (hasInternetConnectivity) {
            progressText.postValue(null);
        } else {
            progressText.postValue(context.getString(R.string.connectivity_changed_no_internet));
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.TIRAMISU)
    public boolean askForNotificationPermission() {
        return ContextCompat.checkSelfPermission(this.getApplication(), Manifest.permission.POST_NOTIFICATIONS) != PackageManager.PERMISSION_GRANTED;
    }

    public String getExitNodeCountry() {
        String cc = preferenceHelper.getExitNodeCountry();
        if (cc == null) {
            return "any";
        }
        Locale[] locales = Locale.getAvailableLocales();
        for (Locale locale : locales) {
            if (cc.equals(locale.getCountry())) {
                return locale.getDisplayCountry();
            }
        }

        return "unknown";
    }
}