#!/usr/bin/env python3
#
# This is a very dummy web server that listens on 127.0.0.1:8080 for incoming
# HTTP requests, responding to them with the client address.

from http.server import HTTPServer, BaseHTTPRequestHandler
import sys
import socket

host = "::"
port = 8080

class Handler(BaseHTTPRequestHandler):
    def do_GET(self):

        self.send_response(200)
        self.send_header("Content-Type", "text/plain")
        self.end_headers()
        self.wfile.write(bytes(self.client_address[0], "utf-8"))

class Server(HTTPServer):
    address_family = socket.AF_INET6

server = Server((host, port), Handler)
server.serve_forever()
