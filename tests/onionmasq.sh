#!/usr/bin/env bash

if [ -z "$CI" ]; then
	NAMESPACES="--mount --mount-proc --fork"
else
	NAMESPACES=""
fi

echo -n "executing unveil $NAMESPACES ... "
unshare $NAMESPACES bash <<:
echo "ok"

set -euo pipefail

echo -n "mounting /tmp ... "
mount -t tmpfs none /tmp
echo "ok"

echo -n "creating onionmasq's /etc/resolv.conf"
echo "nameserver 169.254.42.53" > /tmp/resolv.conf
mount --bind /tmp/resolv.conf /etc/resolv.conf
echo "ok"

echo -n "dropping root privileges ... "
su $SUDO_USER -p -c bash <<::
echo "ok"

set -euo pipefail

echo -n "starting coturn ... "
turnserver -c ./tests/turn.conf &> /dev/null &
turn=\\\$!
echo "ok (pid: \\\$turn)"

echo -n "starting onionmasq with coverage ... "
./maint/with_coverage -o coverage/integration -c ./target/debug/onionmasq -a $CHUTNEY_PATH/net/nodes/arti.toml --disable-fs-permission-checks --turn-server 127.0.0.1:3478 --turn-secret foobar --verbose &> log &
onionmasq=\\\$!
# Wait for a few seconds to let onionmasq start properly.
sleep 5
echo "ok (pid: \\\$onionmasq)"

# Now execute each test.
./tests/dns.sh
./tests/http.sh
./tests/udp.py
./tests/udp6.py
./tests/isolation_cookie.py

# Terminate the inner onionmasq, that is the command spawned by with_coverage,
# but now the with_coverage command itself, which is needed for coverage report
# generation.
echo -n "terminating onionmasq ... "
killall -s SIGINT onionmasq
while ps -p \\\$onionmasq > /dev/null; do
	sleep 1
done
echo "ok"
::
:
