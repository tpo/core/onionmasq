#!/usr/bin/env python3
#
# A UDP integration test.

import socket
import uuid
import threading
import os
import time

INTERFACE = "onion0"
SERVER = ("localhost.home.arpa", 53)

print("creating UDP client socket ... ", end="", flush=True)
s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, 0)
s.setsockopt(socket.SOL_SOCKET, socket.SO_BINDTODEVICE, str(INTERFACE + "\0").encode("utf-8"))
s.bind(("169.254.42.1", 0))
s.connect(SERVER)
print("ok")

datagram = str(uuid.uuid4()).encode("utf-8")
print("sending datagram ... ", end="", flush=True)
assert s.send(datagram) == len(datagram)
print("ok")
