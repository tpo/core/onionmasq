#!/usr/bin/env python3
#
# A UDP integration test.

import socket
import uuid
import threading
import os
import time

INTERFACE = "onion0"
SERVER = (os.environ["DUMMY0_IP4"], 8080)

# A stupid UDP echo server.
def server():
    print("creating IPv4 UDP server socket ... ", end="", flush=True)
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, 0)
    s.bind(SERVER)
    print("ok")

    while True:
        (raw, client) = s.recvfrom(1024)
        s.sendto(raw, client)

# A stupid UDP client.
def client():
    print("creating IPv4 UDP client socket ... ", end="", flush=True)
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, 0)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_BINDTODEVICE, str(INTERFACE + "\0").encode("utf-8"))
    s.bind(("169.254.42.1", 0))
    s.connect(SERVER)
    print("ok")

    datagram = str(uuid.uuid4()).encode("utf-8")
    print("sending IPv4 datagram ... ", end="", flush=True)
    assert s.send(datagram) == len(datagram)
    print("ok")

    print("receiving IPv4 datagram ... ", end="", flush=True)
    assert s.recv(1024) == datagram
    print("ok")

serverThread = threading.Thread(target=server)
serverThread.start()

time.sleep(1)
try:
    client()
except Exception as e:
    print(e)
    os._exit(1)

print("udp IPv4 test was successful")

os._exit(0)
