#!/usr/bin/env bash

set -euo pipefail

echo -n "starting HTTP server ... "
./tests/httpd.py &> /dev/null &
pid=$!
sleep 1
echo "ok (pid: $pid)"

echo -n "performing a simple IPv4 HTTP request ... "
resp=$(curl --interface onion0 --silent "http://$DUMMY0_IP4:8080")
if [[ "$resp" != "::ffff:$DUMMY0_IP4" ]]; then
	echo "error: did not received a ::ffff:$DUMMY0_IP4 back"
	kill -9 $pid
	exit 1
fi
echo "ok"

echo -n "performing a simple IPv6 HTTP request ... "
resp=$(curl --interface onion0 --silent "http://[$DUMMY0_IP6]:8080")
if [[ "$resp" != "$DUMMY0_IP6" ]]; then
	echo "error: did not received a $DUMMY0_IP6 back"
	kill -9 $pid
	exit 1
fi
echo "ok"

echo -n "terminating HTTP server ... "
kill $pid
while ps -p $pid > /dev/null; do
	sleep 1
done
echo "ok"
