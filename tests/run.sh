#!/usr/bin/env bash

if [ -z "$CHUTNEY_PATH" ]; then
	echo "\$CHUTNEY_PATH is undefined, point it to the chtuney directory"
	exit 1
fi
if [ -z "$SUDO_USER" ]; then
	echo "\$SUDO_USER is undefined, you probably want to run this with sudo(1)?"
	exit 1
fi
if [ ! -f "./target/debug/onionmasq" ]; then
	echo "./target/debug/onionmasq does not exist"
	exit 1
fi
# TODO: Check if binary has CAP_NET_ADMIN capability and was compiled with instructions.

if [ -z "$CI" ]; then
	NAMESPACES="--pid --net --fork --mount-proc"
else
	NAMESPACES=""
fi

export DUMMY0_IP4="172.23.39.177"
export DUMMY0_IP6="fd5b:955e:ad4d::"

echo -n "executing unveil $NAMESPACES ... "
unshare $NAMESPACES bash <<:
echo "ok"

set -euo pipefail

echo -n "setting up networking ... "
ip link set lo up

ip link add dummy0 type dummy
ip link set dummy0 up
ip addr add $DUMMY0_IP4/12 dev dummy0
ip addr add $DUMMY0_IP6/48 dev dummy0

ip tuntap add name onion0 mode tun user $SUDO_USER
ip link set onion0 up
ip addr add 169.254.42.1/24 dev onion0
ip addr add fe80::/64 dev onion0
ip route add 10.0.0.0/8 dev onion0
ip -6 route add ::/0 dev onion0
sysctl net.ipv4.conf.onion0.accept_local=1 &> /dev/null
echo "ok"

./tests/chutney.sh &
sleep 10
echo -n "waiting for chutney to bootstrap ... "
sleep 170
echo -n "ok"

./tests/onionmasq.sh
:
