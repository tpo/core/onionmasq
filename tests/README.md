# Integration Tests for Onionmasq

This document tries to document the (very hacky) integration test suite for onionmasq.

Generally, you should **NOT** invoke these scripts here yourself.
Please use the higher level `maint/coverage` script for this purpose.

## Prerequisites

The integration test suite expects you to have an existing binary of onionmasq at
`target/debug/onionmasq` that was compiled with coverage instructions.

Besides, this binary is expected to have the `CAP_NET_ADMIN` capability, which
can be set using `sudo setcap cap_net_admin+ep target/debug/onionmasq`.

There is also a requirements for the following binaries:
* `bash`
* `dig`
* `ip`
* `killall`
* `python3`
* `turnserver` (part of `coturn`)
* `unshare`
* `uuid`
* General POSIX utilities

[chutney](https://gitlab.torproject.org/tpo/core/chutney) is also a requirement.
Please export the `CHUTNEY_PATH` environment variable before running the integration tests.
```sh
export CHUTNEY_PATH=/path/to/chutney/git/clone
```

## How the integration test suite works

Each integration test is a single shell script.
These shell scripts run in isolated containers with a dedicated mount, network and PID namespace.
An integration test may assume there is a working `onion0` interface which listens on `169.254.42.0/24` and `fe80::/10`.
There is also a running DNS resolver on `169.254.42.53:53` and `[fe80::53]:53` respectively.

The core of the integration test is the `run.sh` script.
It performs the following steps:
* Creating a mount, network, and PID namespace
* Mounting a custom `/tmp`
* Overwriting `/etc/resolv.conf` with an empty file
* Setting up the `onion0` interface with respective IP addresses
* Starting chutney at `CHUTNEY_PATH`
* Starting a coturn server
* Starting `onionmasq` behind `maint/with_coverage`
* Executing each and every test
* Stopping the "inner" `onionmasq` to let `maint/with_coverage` generate the report

The script itself requires super user privileges because it creates global system resources.
However, once all privileged actions are done, it drops privileges by switching to the `SUDO_USER` user.
