#!/usr/bin/env bash

set -euo pipefail

dns4="169.254.42.53"
dns6="fe80::53%onion0"

hosts=()
ip4s=()
ip6s=()
nhosts=16

# Generate $nhosts hostnames and populate ip4 and ip6.
echo -n "generating $nhosts random hosts ... "
for i in $(seq 1 $nhosts); do
	host="$(uuid).arpa"
	ip4=$(dig +short @$dns4 A "$host")
	ip6=$(dig +short @$dns4 AAAA "$host")

	hosts+=("$host")
	ip4s+=("$ip4")
	ip6s+=("$ip6")
done
echo "ok"

# Check if the IP addresses persist over time.
echo -n "check if IPs are still constant ... "
for i in "${!hosts[@]}"; do
	host=${hosts[$i]}
	ip4=$(dig +short @$dns4 A "$host")
	ip6=$(dig +short @$dns4 AAAA "$host")

	if [[ "$ip4" != "${ip4s[$i]}" || "$ip6" != "${ip6s[$i]}" ]]; then
		echo "error: IP address changed over time"
		exit 1
	fi

	# Check if IPv6 resolves to the same.
	ip4=$(dig +short @$dns6 A "$host")
	ip6=$(dig +short @$dns6 AAAA "$host")
	if [[ "$ip4" != "${ip4s[$i]}" || "$ip6" != "${ip6s[$i]}" ]]; then
		echo "error: IPv6 resolver yields different result"
		exit 1
	fi
done
echo "ok"

# Check if unsupported DNS record types yield the NOTIMP status.
echo -n "checking unsupported DNS record types ... "
unsupported=("TXT" "SOA" "MX" "HTTPS" "SVCB")
for rtype in "${unsupported[@]}"; do
	host="$(uuid).arpa"
	res=$(dig @$dns4 "$rtype" "$host" | grep -c "status: NOTIMP")
	if [[ "$res" != "1" ]]; then
		echo "error: missing NOTIMP while requesting $rtype"
		exit 1
	fi
done
echo "ok"

echo "dns integration test was successful"
