#!/usr/bin/env bash

if [ -z "$CI" ]; then
	NAMESPACES="--mount --mount-proc --fork"
else
	NAMESPACES=""
fi

echo -n "executing unveil $NAMESPACES ... "
unshare $NAMESPACES bash <<:
echo "ok"

set -euo pipefail

echo -n "mounting /tmp ... "
mount -t tmpfs none /tmp
echo "ok"

echo -n "creating chutney's /etc/resolv.conf"
echo "nameserver 127.0.0.1" > /tmp/resolv.conf
mount --bind /tmp/resolv.conf /etc/resolv.conf
echo "ok"

echo -n "starting dnsmasq ... "
dnsmasq -C ./tests/dnsmasq.conf
echo "ok"

echo -n "dropping root privileges ... "
su $SUDO_USER -p -c bash <<::
echo "ok"

set -euo pipefail

export CHUTNEY_START_TIME=300
echo -n "starting chutney ... "
$CHUTNEY_PATH/chutney configure $CHUTNEY_PATH/networks/ipv6-exit-min &> /dev/null
$CHUTNEY_PATH/chutney start $CHUTNEY_PATH/networks/ipv6-exit-min &> /dev/null
echo "ok"

# Endless loop here, terminate when above PID namespace ceases to exist.
while :; do sleep 1; done

::
:
