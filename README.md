# Onionmasq - Masquerading Onions

This project is an attempt to implement a simple user-space network stack that
can handle TCP and UDP state such that it is possible to forward the traffic
into the Tor network.

## Support

We are hanging out on `#tor-vpn` on the IRC network OFTC. You can also reach this channel via Matrix using `#tor-vpn:matrix.org`.

## Android Development

1. Download [Android Studio](https://developer.android.com/studio) and install
   it. Once it's installed, open it, and follow the setup wizard. Once you are
   in the development environment, create an empty project and then go to
   `Tools` then `SDK Manager`. Here you need to select `NDK (side-by-side)`,
   `Android SDK Platform-Tools`, `Android SDK Build-Tools`, and `Android SDK
   Command-line Tools`. Go get a cup of tea.

2. Please follow the [instructions](https://rustup.rs/) to get the `rustup` tool
   for managing your Rust toolchains -- you will need multiple toolchains, so it
   is unlikely you can use your distribution provided Rust packages.

3. Once you have `rustup` available, please make sure you have all required targets available using:

    `$ ./install-toolchains.sh`

4. Install `cargo-ndk` using

    `$ cargo install cargo-ndk`

5. Set the environment variables necessary to find the Android SDK/NDK bits
    (note, your ndk version may be different from the example below):
    `export ANDROID_HOME=~/Android/Sdk`
    `export ANDROID_NDK_HOME=~/Android/Sdk/ndk/25.2.9519653/`

    (alternatively, place these in your shell environment initialization files)

6. Try to build `onionmasq.so`'s using

    `./build-ndk.sh`
    (you can specify x86, x86_64, armeabi-v7a and arm64-v8 if you want to build only particular targets and speed up the build)

   If you want more detailed logging (for development only!), specify `VERBOSE=1 ./build_ndk.sh`. (This is disabled by
   default to avoid sending potentially sensitive connection logs to the system logcat.)

7. Create the onionmasq aar lib:

    `$ cd android/OnionmasqAndroid`
    `$ ./gradlew build`

8. Create the Onionmasq VPN sample app:

    `$ ./gradlew assembleDebug`

## Running outside Android

The VPN logic can also be run outside the Android app, on a standalone Linux
computer. Run the `onionmasq` crate to give this a go:

    $ cargo run -p onionmasq

On first run, this will complain of a missing TUN interface.
Create this `onion0` interface using:

    $ sudo ip tuntap add name onion0 mode tun user $(whoami)
    $ sudo ip link set onion0 up
    $ sudo ip addr add 169.254.42.1/24 dev onion0
    $ sudo ip -6 route add ::/0 dev onion0

To route traffic to a specific IP:

    $ ip route add <IP>/32 via 169.254.42.2 dev onion0

To remove it:

    $ ip route del <IP>/32 via 169.254.42.2 dev onion0

### Additional notes for macOS Apple Silicon users

You can't run the `onionmasq` crate on macOS yet. However, you can set up
a Linux VM and run it inside there for a nicer development experience.

(I set up my VM with [this UTM Ubuntu guide](https://docs.getutm.app/guides/ubuntu/),
but you don't have to.)

In order to cross compile from macOS to Linux, first install
[a cross compiler](https://github.com/FiloSottile/homebrew-musl-cross):

    $ brew install filosottile/musl-cross/musl-cross --with-aarch64

Then, you should be able to install the target and build the crate:

    $ rustup target add aarch64-unknown-linux-musl
    $ cargo build -p onionmasq --target aarch64-unknown-linux-musl

Copy the resulting binary to your Linux VM over SSH (example):

    $ scp target/aarch64-unknown-linux-musl/debug/onionmasq <address of Linux VM>:~/

## Resources

For further information about Arti and Android, please see the [Arti documentation](https://gitlab.torproject.org/tpo/core/arti/-/blob/main/doc/Android.md)

## License

This code is licensed under either of

 * [Apache License, Version 2.0](https://www.apache.org/licenses/LICENSE-2.0)
 * [MIT license](https://opensource.org/licenses/MIT)

at your option.

## Contribution

Unless you explicitly state otherwise, any contribution intentionally
submitted for inclusion in the work by you, as defined in the Apache-2.0
license, shall be dual licensed as above, without any additional terms or
conditions.
